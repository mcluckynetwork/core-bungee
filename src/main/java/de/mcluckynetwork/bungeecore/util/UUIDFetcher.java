package de.mcluckynetwork.bungeecore.util;

import de.mcluckynetwork.bungeecore.Main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UUIDFetcher {

   private static Map<String, UUID> uuidCache = new HashMap<>();
   private static Map<UUID, String> nameCache = new HashMap<>();

   public static UUID getUUID(String name) {
      if(uuidCache.containsKey(name)) {
         return uuidCache.get(name);
      }
      ResultSet rs = Main.getMySQL().Query("SELECT * FROM uuids WHERE name='" + name + "'");
      try{
         if(rs.next()) {
            String id = rs.getString("uuid");
            UUID uuid = UUID.fromString(id);
            uuidCache.put(name, uuid);
            nameCache.put(uuid, name);
            return uuid;
         }
      }catch(SQLException ex) {
         ex.printStackTrace();
      }
      return null;
   }

   public static String getName(UUID uuid) {
      if (nameCache.containsKey(uuid)) {
         return nameCache.get(uuid);
      }
      ResultSet rs = Main.getMySQL().Query("SELECT * FROM uuids WHERE uuid='" + uuid + "'");
      try{
         if(rs.next()) {
            String name = rs.getString("name");
            nameCache.put(uuid, name);
            uuidCache.put(name, uuid);
            return name;
         }
      }catch(SQLException ex) {
         ex.printStackTrace();
      }
      return null;
   }

}

