package de.mcluckynetwork.bungeecore.util.communication;

import de.mcluckynetwork.bungeecore.netty.Protocol;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.charset.Charset;

public class ChatClient {

	private static Channel c;
	private static NioEventLoopGroup child;

	public ChatClient() throws Exception {
		child = new NioEventLoopGroup();

		Bootstrap bootstrap = new Bootstrap().group(child).channel(NioSocketChannel.class).handler(new ChannelInitializer<Channel>() {

			@Override
			protected void initChannel(Channel c) throws Exception {
				c.pipeline().addLast(new StringEncoder(Charset.forName("UTF-8"))).addLast(new LineBasedFrameDecoder(1024)).addLast(new StringDecoder(Charset.forName("UTF-8"))).addLast(new ChatClientChannelHandler());
			}

		});
		c = bootstrap.connect("localhost", 6666).sync().channel();
	}

	public static void close() {
		child.shutdownGracefully();
	}

	public static void sendPacket(Packet packet) {
		String msg = packet.write();
		msg = msg.replace("§", "&");
		int id = Protocol.getIDFromClass(packet.getClass());
		c.writeAndFlush(id + "#" + msg + "\n");
	}

}
