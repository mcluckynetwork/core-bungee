package de.mcluckynetwork.bungeecore.util.communication;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.MuteClient;
import de.mcluckynetwork.bungeecore.netty.Protocol;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerOnline;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerStatusUpdate;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.*;
import de.mcluckynetwork.bungeecore.util.BroadcastMessage;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.Lobby;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.LobbyManager;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem.GameServer;
import de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem.GameServerManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.net.InetSocketAddress;

public class ChatClientChannelHandler extends SimpleChannelInboundHandler<String> {
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String rmsg) throws Exception {
		String message = rmsg.replace("&", "§");
		String[] msg = message.split("#");
		String rmessage = msg[1];
		int pid = Integer.valueOf(msg[0]);
		Class<? extends Packet> packetClass = Protocol.getClassFromID(pid);
		Packet packet = null;
		if(packetClass != null){
			packet = packetClass.newInstance();
			packet.read(rmessage);
		}
		if(packet instanceof PacketServerOnline) {
			PacketServerOnline p = (PacketServerOnline) packet;
			if(p.getServerType() == ServerType.LOBBY || p.getServerType() == ServerType.SILENT) {
				Lobby lobby = LobbyManager.getLobby(p.getName());
				if (lobby != null) {
					LobbyManager.lobbies.remove(lobby);
				}
				LobbyManager.lobbies.add(new Lobby(p.getName(), p.getOnline(), p.getMaximal(), p.getState(), p.getServerType(), ctx.channel()));
			}else if(p.getServerType() == ServerType.GAME) {
				GameServer server = GameServerManager.getServer(p.getName());
				if(server != null) {
					GameServerManager.removeServer(server);
				}
				GameServerManager.addServer(new GameServer(p.getName(), p.getMaximal(), p.getState()));
			}
		}else if(packet instanceof PacketServerStatusUpdate) {
			PacketServerStatusUpdate p = (PacketServerStatusUpdate) packet;
			if(p.getServerType() == ServerType.LOBBY || p.getServerType() == ServerType.SILENT) {
				if(p.getState() != State.OFFLINE) {
					Lobby l = LobbyManager.getLobby(p.getName());
					if(l != null) {
						l.setStatus(p.getState());
					}else{
						Main.sendErrorMessage("§7Lobby §e" + p.getName() + " §7ist nicht registriert");
					}
				}else{
					Lobby lobby = LobbyManager.getLobby(p.getName());
					if(lobby != null) {
						LobbyManager.lobbies.remove(lobby);
					}else{
						Main.sendErrorMessage("§7Lobby §e" + p.getName() + " §7ist nicht registriert");
					}
				}
			}else{
				if(p.getState() != State.OFFLINE) {
					GameServer gs = GameServerManager.getServer(p.getName());
					if(gs != null) {
						gs.setState(p.getState());
					}else{
						Main.sendErrorMessage("§7GameServer §e" + p.getName() + " §7ist nicht registriert");
					}
				}else{
					GameServer server = GameServerManager.getServer(p.getName());
					if(server != null) {
						GameServerManager.removeServer(server);
					}else{
						Main.sendErrorMessage("§7GameServer §e" + p.getName() + " §7ist nicht registriert");
					}
				}
			}
		}else if(packet instanceof PacketTeamMessage) {
			PacketTeamMessage p = (PacketTeamMessage) packet;
			if(p.getSender().equalsIgnoreCase("System")) {
				Main.sendRawTeamMessage(p.getSender(), "§7" + p.getMessage());
			}else {
				Client c = Main.getOfflineClient(UUIDFetcher.getUUID(p.getSender()), p.getSender());
				if(c != null) {
					Main.sendRawTeamMessage(c.getChatName(), "§7" + p.getMessage());
				}else{
					Main.sendErrorMessage("§7Couldn't §7load §7offline §7Client §7by §e" + p.getSender());
				}
			}
		}else if(packet instanceof PacketFriendAction) {
			PacketFriendAction p = (PacketFriendAction) packet;
			Main.online = p.getOnline();
			for(FriendSystem.Friend friend : FriendSystem.getFriends(p.getSender())) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(friend.getName());
				if(player != null) {
					if(p.getAction() == PacketFriendAction.Action.JOIN) {
						player.sendMessage(new TextComponent(Main.fpre + Main.getOfflineClient(UUIDFetcher.getUUID(p.getSender()), p.getSender()).getChatName() + " §7ist nun §aonline"));
					}else{
						player.sendMessage(new TextComponent(Main.fpre + Main.getOfflineClient(UUIDFetcher.getUUID(p.getSender()), p.getSender()).getChatName() + " §7ist nun §coffline"));
					}
				}
			}
		}else if(packet instanceof PacketFriendMsg) {
			PacketFriendMsg p = (PacketFriendMsg) packet;
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p.getReceiver());
			if(player != null) {
				Client t = Main.getClient(p.getReceiver());
				t.setRet(ChatColor.stripColor(p.getSender()));
				player.sendMessage(new TextComponent(Main.fpre + p.getSender() + "§7 -> " + t.getChatName() + "§8: §7" + p.getMessage()));
			}
		}else if(packet instanceof PacketBroadcastMessage) {
			PacketBroadcastMessage p = (PacketBroadcastMessage) packet;
			ProxyServer.getInstance().broadcast(new TextComponent("§7[§4WICHTIG§7] " + p.getMessage()));
		}else if(packet instanceof PacketMotdUpdate) {
			PacketMotdUpdate p = (PacketMotdUpdate) packet;
			Main.motd = p.getMotd();
		}else if(packet instanceof PacketPlayerAction) {
			PacketPlayerAction p = (PacketPlayerAction) packet;
			if(p.getAction() == PacketPlayerAction.PlayerAction.MUTE) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p.getPlayer());
				if(player != null) {
					Client c = Main.getClient(player);
					MuteClient mc = c.getMuteClientFromMySQL(c.getUuid());
					if(mc != null) {
						c.setMuteClient(mc);
						c.setMuted(true);
					}else{
						c.setMuted(false);
					}
				}
			}else if(p.getAction() == PacketPlayerAction.PlayerAction.KICK) {
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p.getPlayer());
				if(player != null) {
					if (p.getReason().equalsIgnoreCase("NoReason")) {
						player.disconnect(new TextComponent(Main.pre + "\n" + "§7Du §7wurdest §7vom §7Netzwerk §7gekickt"));
					} else {
						player.disconnect(new TextComponent(Main.pre + "\n" + "§7Du §7wurdest §7vom §7Netzwerk §7gekickt \n§7Grund: §e" + msg));
					}
				}
			}else{
				ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p.getPlayer());
				if(player != null) {
					String disconnectBanMessage = BanSystem.disconnectBanMessage(player.getUniqueId(), player.getName());
					player.disconnect(new TextComponent(disconnectBanMessage));
				}
			}
		}else if(packet instanceof PacketSlotUpdate) {
			Main.maximal = ((PacketSlotUpdate) packet).getSlots();
		}else if(packet instanceof PacketFriendActionPerform) {
			PacketFriendActionPerform p = (PacketFriendActionPerform) packet;
			if(p.getAction() == PacketFriendActionPerform.FriendAction.ACCEPT) {
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getReceiver());
				if(target != null) {
					target.sendMessage(new TextComponent(Main.fpre + p.getSender() + " §7hat §7deine §7Anfrage §7angenommen"));
				}
			}else if(p.getAction() == PacketFriendActionPerform.FriendAction.DENY) {
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getReceiver());
				if(target != null) {
					target.sendMessage(new TextComponent(Main.fpre + p.getSender() + " §7hat §7deine §7Anfrage §7abgelehnt"));
				}
			}else if(p.getAction() == PacketFriendActionPerform.FriendAction.REMOVE) {
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getReceiver());
				if(target != null) {
					target.sendMessage(new TextComponent(Main.fpre + "§7Der §7Spieler " + p.getSender() + " §7hat §7dich §7von §7seinen §7Freunden §7entfernt"));
				}
			}else{
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getReceiver());
				if(target != null) {
					target.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7eine §7Anfrage §7von " + p.getSender() + " §7erhalten"));
					TextComponent accept = new TextComponent(Main.fpre + "§7Annehmen: §e/friend §eaccept " + p.getSender());
					TextComponent deny = new TextComponent(Main.fpre + "§7Ablehnen: §e/friend §edeny " + p.getSender());
					accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + ChatColor.stripColor(p.getSender())));
					deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + ChatColor.stripColor(p.getSender())));
					target.sendMessage(accept);
					target.sendMessage(deny);
				}
			}
		}else if(packet instanceof PacketBMMessage) {
			PacketBMMessage p = (PacketBMMessage) packet;
			if(p.getAction() == PacketBMMessage.BMAction.ADD) {
				Main.messages.add(new BroadcastMessage(Main.messages.size(), p.getMessage()));
			}else{
				BroadcastMessage t = null;
				for(BroadcastMessage bm : Main.messages) {
					if(bm.getMessage().equalsIgnoreCase(p.getMessage())) {
						t = bm;
					}
				}
				if(t != null) {
					Main.messages.remove(t);
				}
			}
		}else if(packet instanceof PacketServerRequest) {
			PacketServerRequest p = (PacketServerRequest) packet;
			System.out.println("[INFO] Received PacketServerRequest");
			if(p.getServer().equalsIgnoreCase("Unknown")) {
				if (Main.isPlayerOnline(p.getPlayer())) {
					p.setServer(ProxyServer.getInstance().getPlayer(p.getPlayer()).getServer().getInfo().getName());
					ChatClient.sendPacket(p);
					System.out.println("[INFO] Set Server for " + p.getPlayer() + ": " + p.getServer());
				}
			}else{
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(p.getReceiver());
				if(target != null) {
					if (p.getAction() == PacketServerRequest.ServerAction.GOTO) {
						System.out.println("[INFO] Got Server for " + p.getPlayer() + ": " + p.getServer());
						target.connect(ProxyServer.getInstance().getServerInfo(p.getServer()));
						Client c = Main.getOfflineClient(UUIDFetcher.getUUID(p.getPlayer()), p.getPlayer());
						if(c != null) {
							target.sendMessage(new TextComponent(Main.pre + "§7Du §7wurdest §7zu " + c.getChatName() + " §7gemoved"));
						}else{
							Main.sendErrorMessage("§7Couldn't §7load §7offline §7Client §7by §e" + p.getPlayer());
						}
					} else {
						System.out.println("[INFO] Got Server for " + p.getPlayer() + ": " + p.getServer());
						TextComponent server = new TextComponent("§e" + p.getServer());
						server.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + p.getServer()));
						Client c = Main.getOfflineClient(UUIDFetcher.getUUID(p.getPlayer()), p.getPlayer());
						if(c != null) {
							target.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler " + c.getChatName() + " §7ist §7auf §7dem §7Server "), server);
						}else{
							Main.sendErrorMessage("§7Couldn't §7load §7offline §7Client §7by §e" + p.getPlayer());
						}
					}
				}
			}
		}else if(packet instanceof PacketNetworkPlayerOnline) {
			PacketNetworkPlayerOnline p = (PacketNetworkPlayerOnline) packet;
			Main.online = p.getOnline();
		}else if(packet instanceof PacketNetworkStop) {
			ProxyServer.getInstance().stop();
		}else if(packet instanceof PacketPlayerReport) {
			PacketPlayerReport p = (PacketPlayerReport) packet;
			TextComponent component = new TextComponent(Main.rpre);
			TextComponent click = new TextComponent(p.getReported());
			click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/goto " + ChatColor.stripColor(p.getReported())));
			component.addExtra(click);
			component.addExtra(new TextComponent(" §7wurde §7von " + p.getReporter() + " §7reportet"));
			Main.sendReportMessage(component);
			Main.sendReportMessage(Main.rpre + "§7Grund: §e" + p.getReason() + " §7| §7Total: §e" + p.getTotal() + " §7| Laufzeit: §e" + p.getRuntime());
		}else if(packet instanceof PacketGlobalState) {
			PacketGlobalState p = (PacketGlobalState) packet;
			Main.state = p.getProxyState();
			if(Main.state == ProxyState.MAINTENANCE) {
				for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
					Client a = Main.getClient(all);
					if (!a.getRang().isTeam()) {
						all.disconnect(new TextComponent(Main.pre + "§7Du kannst das Netzwerk momentan nicht betreteten \n§cGrund: §4WARTUNGSARBEITEN"));
					}
				}
			}
		}else if(packet instanceof PacketPlayerReportJoin) {
			PacketPlayerReportJoin p = (PacketPlayerReportJoin) packet;
			TextComponent component = new TextComponent(Main.rpre);
			TextComponent click = new TextComponent(p.getReported());
			click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/goto " + ChatColor.stripColor(p.getReported())));
			component.addExtra(click);
			component.addExtra(new TextComponent(" §7ist §7nun §7wieder §7online"));
			Main.sendReportMessage(component);
			Main.sendReportMessage(Main.rpre + "§7Er §7wurde §e" + p.getTotal() + " §7mal §7reportet");
		}else if(packet instanceof PacketServerInfo) {
			PacketServerInfo p = (PacketServerInfo) packet;
			if(p.getAction() == PacketServerInfo.ServerAction.ADD) {
				ServerInfo s = ProxyServer.getInstance().constructServerInfo(p.getName(), new InetSocketAddress(p.getHost(),p.getPort()), "", false);
				ProxyServer.getInstance().getServers().put(p.getName(), s);
			}else{
				if(ProxyServer.getInstance().getServers().containsKey(p.getName())) {
					ProxyServer.getInstance().getServers().remove(p.getName());
				}
			}
		}else if(packet instanceof PacketPerformCommand) {
			PacketPerformCommand p = (PacketPerformCommand) packet;
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p.getSender());
			if(player != null) {
				ProxyServer.getInstance().getPluginManager().dispatchCommand(player, p.getCommand());
				System.out.println("[INFO] Performing Command " + p.getCommand() + " for " + player.getName());
			}
		}else if(packet instanceof PacketBlackList) {
			PacketBlackList p = (PacketBlackList) packet;
			if(p.getAction() == PacketBlackList.BlackListAction.ADD) {
				if(!Main.blacklist.contains(p.getMessage()))  {
					Main.blacklist.add(p.getMessage());
				}
			}else{
				if(Main.blacklist.contains(p.getMessage())) {
					Main.blacklist.remove(p.getMessage());
				}
			}
		}
	}
}
