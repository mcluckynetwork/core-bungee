package de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem;

import de.mcluckynetwork.bungeecore.netty.ServerType;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Robin on 03.09.2015.
 */
public class LobbyManager {

    public static ArrayList<Lobby> lobbies = new ArrayList<>();

    public static Lobby getRandomLobby() {
        Random r = new Random();
        int rnd = r.nextInt(lobbies.size());
        Lobby l = lobbies.get(rnd);
        if(l.getType() == ServerType.LOBBY) {
            return l;
        }else{
            return getRandomLobby();
        }
    }

    public static Lobby getLobby(String name) {
        for(Lobby l : lobbies) {
            if(l.getName().equalsIgnoreCase(name)) {
                return l;
            }
        }
        return null;
    }

}
