package de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 06.11.2015.
 */
public class GameServerManager {

    private static List<GameServer> serverList = new ArrayList<>();

    public static void addServer(GameServer s) {
        serverList.add(s);
    }

    public static void removeServer(GameServer s) {
        serverList.remove(s);
    }

    public static GameServer getServer(String name) {
        for(GameServer s : serverList) {
            if(s.getName().equalsIgnoreCase(name)) {
                return s;
            }
        }
        return null;
    }

}
