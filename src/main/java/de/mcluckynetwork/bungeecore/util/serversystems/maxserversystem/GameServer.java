package de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem;

import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Robin on 06.11.2015.
 */
@Getter
public class GameServer {

    private String name;
    private int maximal;
    @Setter
    private State state;

    public GameServer(String name, int maximal, State state) {
        this.name = name;
        this.maximal = maximal;
        this.state = state;
    }

}
