package de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem;

import lombok.Getter;

/**
 * Created by Robin on 03.09.2015.
 */
public enum State {

    ONLINE(0, "§2ONLINE"), WARTEN(1, "§2WARTEN"), LOBBY(2, "§2LOBBY"), FULL(3, "§6LOBBY"), SPAWN(4, "§7INGAME"), WARMUP(5, "§7INGAME"), INGAME(6, "§7INGAME"), DEATHMATCH(7, "§7INGAME"), RESTART(8, "§cRESTART"), OFFLINE(9, "§cRESTART"), MAINTENANCE(10, "§cOFFLINE");

    private int id;
    @Getter
    private String name;

    State(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getID() {
        return this.id;
    }

    public static State fromID(int id) {
        for(State s : values()) {
            if(s.getID() == id) {
                return s;
            }
        }
        return OFFLINE;
    }

}