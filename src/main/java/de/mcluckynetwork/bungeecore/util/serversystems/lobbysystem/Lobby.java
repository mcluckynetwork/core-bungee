package de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem;

import de.mcluckynetwork.bungeecore.netty.ServerType;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

/**
 * Created by Robin on 03.09.2015.
**/
@Getter
@Setter
public class Lobby {

    private String name;
    private int online;
    private int maximal;
    private State status;
    private ServerType type;
    private Channel channel;

    public Lobby(String server, int online, int maximal, State status, ServerType type, Channel ch) {
        this.type = type;
        this.name = server;
        this.online = online;
        this.maximal = maximal;
        this.status = status;
        this.channel = ch;
    }

    public ServerInfo getServer() {
        return ProxyServer.getInstance().getServerInfo(this.name);

    }
}
