package de.mcluckynetwork.bungeecore.util.exception;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Robin on 11.10.2015.
 */
public class ExceptionManager {

    public static HashMap<UUID, PluginException> exceptions = new HashMap<>();

    public static void addException(UUID uuid, PluginException ex) {
        exceptions.put(uuid, ex);
    }

    public static PluginException getException(UUID uuid) {
        if(isException(uuid)) {
            return exceptions.get(uuid);
        }
        return null;
    }

    public static boolean isException(UUID uuid) {
        if(exceptions.containsKey(uuid)) {
            return true;
        }
        return false;
    }

}
