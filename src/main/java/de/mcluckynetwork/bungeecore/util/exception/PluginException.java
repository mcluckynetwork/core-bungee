package de.mcluckynetwork.bungeecore.util.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Robin on 11.10.2015.
 */
@Setter
@Getter
public class PluginException {

    private ExceptionType type;
    private StackTraceElement[] elements;
    private UUID uuid;
    private List<String> debug;

    public PluginException(ExceptionType type, StackTraceElement[] elements, UUID uuid) {
        this.type = type;
        this.elements = elements;
        this.uuid = uuid;
        this.debug = new ArrayList<>();
    }

    public PluginException(ExceptionType type) {
        this.type = type;
        this.debug = new ArrayList<>();
    }

    public void addMessage(String msg) {
        debug.add(msg);
    }

}
