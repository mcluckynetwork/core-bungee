package de.mcluckynetwork.bungeecore.util.exception;

import lombok.Getter;

/**
 * Created by Robin on 11.10.2015.
 */
@Getter
public enum ExceptionType {

    COMMAND("Command", 1),

    EVENT("Event", 2),

    METHOD("Methode", 3);

    private String name;
    private int id;

    private ExceptionType(String name, int id) {
        this.id = id;
        this.name = name;
    }

}
