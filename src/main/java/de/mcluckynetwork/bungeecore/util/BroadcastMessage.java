package de.mcluckynetwork.bungeecore.util;

import lombok.Getter;

/**
 * Created by Robin on 28.12.2015.
 */
@Getter
public class BroadcastMessage {

    private int id;
    public String message;

    public BroadcastMessage(int id, String message) {
        this.id = id;
        this.message = message;
    }

}
