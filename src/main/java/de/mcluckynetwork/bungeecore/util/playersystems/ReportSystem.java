package de.mcluckynetwork.bungeecore.util.playersystems;

import de.mcluckynetwork.bungeecore.Main;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Robin on 30.12.2015.
 */
public class ReportSystem {

    private static List<Report> reports = new ArrayList<>();

    public static void addReport(Report report) {
        reports.add(report);
    }

    public static void removeReport(Report report) {
        reports.remove(report);
    }

    public static Report getReport(String reporter, String reported) {
        for(Report report : reports) {
            if(report.getReported().equalsIgnoreCase(reported) && report.getReporter().equalsIgnoreCase(reporter)) {
                return report;
            }
        }
        return null;
    }

    public static void completeReport(UUID reportedid) {
        try {
            ResultSet rs = Main.getMySQL().Query("SELECT runtime FROM nreports WHERE uuid='" + reportedid + "'");
            if (rs.next()) {
                Main.getMySQL().Update("UPDATE nreports SET total = total + 1, runtime = runtime + 1 WHERE uuid='" + reportedid + "'");
                return;
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        Main.getMySQL().Update("INSERT INTO nreports(uuid, total, runtime) VALUES('" + reportedid + "' , '" + 1 + "' , '" + 1 + "')");
    }

    public static void clearReport(ProxiedPlayer target) {
        try {
            ResultSet rs = Main.getMySQL().Query("SELECT runtime FROM nreports WHERE uuid='" + target.getUniqueId() + "'");
            if (rs.next()) {
                Main.getMySQL().Update("UPDATE nreports SET runtime = 0 WHERE uuid='" + target.getUniqueId() + "'");
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static int getTotalReports(UUID uuid) {
        try{
            ResultSet rs = Main.getMySQL().Query("SELECT total FROM nreports WHERE uuid='" + uuid + "'");
            if(rs.next()) {
                return rs.getInt("total");
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static int[] getReportInfos(UUID uuid) {
        int[] toreturn = new int[2];
        try{
            ResultSet rs = Main.getMySQL().Query("SELECT * FROM nreports WHERE uuid='" + uuid + "'");
            if(rs.next()) {
                toreturn[0] = rs.getInt("total");
                toreturn[1] = rs.getInt("runtime");
                return toreturn;
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        toreturn[0] = 0;
        toreturn[1] = 0;
        return toreturn;
    }

}
