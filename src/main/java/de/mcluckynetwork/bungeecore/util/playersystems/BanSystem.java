package de.mcluckynetwork.bungeecore.util.playersystems;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.MuteClient;

public class BanSystem {

	//BAN

	public static boolean isInBanTable(UUID uuid) {
		try{
			ResultSet rs = Main.getMySQL().Query("SELECT banart FROM banusers WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String[] getAktivBanInfos(UUID uuid) {
		String[] toreturn = new String[4];
		String banart = null;
		String reason = null;
		String WhoBanned = null;
		int time = 0;
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT * FROM banusers WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				System.out.println("abfragen");
				WhoBanned = rs.getString("whouuid");
				banart = rs.getString("banart");
				reason = rs.getString("reason");
				time = rs.getInt("time");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		toreturn[0] = WhoBanned;
		toreturn[1] = banart;
		toreturn[2] = reason;
		toreturn[3] = "" + time;
		return toreturn;
	}

	public static String disconnectBanMessage(UUID uuid, String name) {
		String toreturn = null;
		boolean inTable = false;
		String banart = null;
		String reason = null;
		String WhoBanned = null;
		int time = 0;
		boolean permanent = false;;
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT * FROM banusers WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				System.out.println("abfragen");
				WhoBanned = rs.getString("whouuid");
				banart = rs.getString("banart");
				reason = rs.getString("reason");
				time = rs.getInt("time");
				inTable = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(inTable) {
			if(time <= 0) {
				permanent = true;
			}
			int value = (int) (time - (System.currentTimeMillis() / 1000));
			String[] binfos = banart.split(",");
			if(binfos[0].equalsIgnoreCase("ban")) {
				if(value > 0) {
					int[] zeit = secondsToArrays(value);
					toreturn = Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7Verbleibende Zeit: §e" + zeit[0] + " §7Monate §e" + zeit[1] + " §7Tage §e" + zeit[2] + " §7Stunden §e" + zeit[3] + " §7Minuten §e" + zeit[4] + " §7Sekunden \n§7Entbannungsantrag: http://www.luckyworld.eu";
				}else{
					if(permanent) {
						toreturn = Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7 Zeit: §4PERMANENT \n§7Entbannungsantrag: http://luckyworld.eu";
					}else{
						String[] infos = banart.split(",");
						String art = infos[0];
						String dauer = infos[1];
						setBanHistory(uuid, InfosToBanHistory(art, WhoBanned, dauer, reason));
						Main.getMySQL().Update("DELETE FROM banusers WHERE uuid = '" + uuid + "'");
						toreturn = null;
					}
				}
			}else{
				toreturn = null;
			}
		}else{
			toreturn = null;
		}
		return toreturn;
	}

	public static void unbanPlayerBan(UUID uuid, boolean shorter) {
		ResultSet rs = Main.getMySQL().Query("SELECT * FROM banusers WHERE uuid = '" + uuid + "'");
		try{
			while(rs.next()) {
				String WhoBanned = rs.getString("whouuid");
				String banart = rs.getString("banart");
				String oreason = rs.getString("reason");
				String[] infos = banart.split(",");
				String art = infos[0];
				String dauer = infos[1];
				if(shorter) {
					setBanHistory(uuid, InfosToBanHistory(art, WhoBanned, dauer, oreason + ";Verkürzt"));
				}else{
					setBanHistory(uuid, InfosToBanHistory(art, WhoBanned, dauer, oreason));
				}
				Main.getMySQL().Update("DELETE FROM banusers WHERE uuid = '" +uuid + "'"); //xyz
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public static void manualBanPlayerBan(UUID WhoBanned, UUID uuid, int time, String reason, String rohtime) {
		try {
			ResultSet report = Main.getMySQL().Query("SELECT * FROM nreports WHERE uuid='" + uuid + "'");
			if (report.next()) {
				Main.getMySQL().Update("DELETE FROM nreports WHERE uuid='" + uuid + "'");
			}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		if(!isInBanTable(uuid)) {
			Main.getMySQL().Update("INSERT INTO banusers(uuid,whouuid,banart,reason,time) values ('" + uuid + "' , '" + WhoBanned + "' , '" + "Ban,"+ rohtime + "', '" + reason + "' , '" + time + "')");
		}else{
			ResultSet rs = Main.getMySQL().Query("SELECT * FROM banusers WHERE uuid = '" + uuid + "'");
			try{
				while(rs.next()) {
					String who = rs.getString("whouuid");
					String banart = rs.getString("banart");
					String oreason = rs.getString("reason");
					String[] infos = banart.split(",");
					String art = infos[0];
					String dauer = infos[1];
					setBanHistory(uuid, InfosToBanHistory(art, who, dauer, oreason));
					Main.getMySQL().Update("DELETE FROM banusers WHERE uuid = '" + uuid + "'");
					Main.getMySQL().Update("INSERT INTO banusers(uuid,whouuid,banart,reason,time) values ('" + uuid + "' , '" + WhoBanned + "' , '" + "Ban," + rohtime + "', '" + reason + "' , '" + time + "')");
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//CHATBAN
	public static boolean isInChatBanTable(UUID uuid) {
		try{
			ResultSet rs = Main.getMySQL().Query("SELECT banart FROM chatbanusers WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String[] getAktivChatBanInfos(UUID uuid) {
		String[] toreturn = new String[4];
		String banart = null;
		String reason = null;
		String WhoBanned = null;
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT * FROM chatbanusers WHERE uuid = '" + uuid + "'");
			while (rs.next()) {
				System.out.println("abfragen");
				WhoBanned = rs.getString("whouuid");
				banart = rs.getString("banart");
				reason = rs.getString("reason");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		toreturn[0] = WhoBanned;
		toreturn[1] = banart;
		toreturn[2] = reason;
		return toreturn;
	}

	public static void unbanPlayerChatBan(UUID uuid) {
		ResultSet rs = Main.getMySQL().Query("SELECT * FROM chatbanusers WHERE uuid = '" + uuid + "'");
		try{
			while(rs.next()) {
				String WhoBanned = rs.getString("whouuid");
				String banart = rs.getString("banart");
				String oreason = rs.getString("reason");
				String[] infos = banart.split(",");
				String art = infos[0];
				String dauer = infos[1];
				setBanHistory(uuid, InfosToBanHistory(art, WhoBanned, dauer, oreason));
				Main.getMySQL().Update("DELETE FROM chatbanusers WHERE uuid = '" + uuid + "'");
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public static void manualChatBanPlayerBan(UUID WhoBanned, UUID name, int time, String reason, String rohtime) {
		if(!isInChatBanTable(name)) {
			Main.getMySQL().Update("INSERT INTO chatbanusers(uuid,whouuid,banart,reason,time) values ('" + name + "' , '" + WhoBanned + "' , '" + "ChatBan," + rohtime + "', '" + reason + "' , '" + time + "')");
		}else{
			ResultSet rs = Main.getMySQL().Query("SELECT * FROM chatbanusers WHERE uuid = '" + name + "'");
			try{
				while(rs.next()) {
					String who = rs.getString("whouuid");
					String banart = rs.getString("banart");
					String oreason = rs.getString("reason");
					String[] infos = banart.split(",");
					String art = infos[0];
					String dauer = infos[1];
					setBanHistory(name, InfosToBanHistory(art, who, dauer, oreason));
					Main.getMySQL().Update("DELETE FROM chatbanusers WHERE uuid = '" + name + "'");
					Main.getMySQL().Update("INSERT INTO chatbanusers(uuid,whouuid,banart,reason,time) values ('" + name + "' , '" + WhoBanned + "' , '" + "ChatBan," + rohtime + "' , '" + reason + "' , '" + time + "')");
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static int[] secondsToArrays(int seconds) {
		int[] time = new int[5];
		int months = (int) (Math.floor(seconds / 2592000.0D));
		int rest = seconds - months * 2592000;
		int days = (int) (Math.floor(rest / 86400.0D));
		rest = rest - days * 86400;
		int hours = (int) (Math.floor(rest / 3600.0D));
		rest = rest - hours * 3600;
		int minutes = (int) (Math.floor(rest / 60.0D));
		rest = rest - minutes * 60;
		time[0] = months;
		time[1] = days;
		time[2] = hours;
		time[3] = minutes;
		time[4] = rest;
		return time;
	}

	//BAN-HISTORY

	public static boolean isInHistoryTable(UUID uuid) {
		try{
			ResultSet rs = Main.getMySQL().Query("SELECT bandatas FROM banusershistory WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				return true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String getBanHistory(UUID uuid) {
		try{
			ResultSet rs = Main.getMySQL().Query("SELECT bandatas FROM banusershistory WHERE uuid = '" + uuid + "'");
			while(rs.next()) {
				return rs.getString(1);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void setBanHistory(UUID uuid, String history) {
		if(getBanHistory(uuid) != null) {
			Main.getMySQL().Update("UPDATE banusershistory SET bandatas = '" + getBanHistory(uuid) + "#" + history + "' WHERE uuid = '" + uuid + "'");
		}else{
			Main.getMySQL().Update("INSERT INTO banusershistory (uuid,bandatas) values ('" + uuid + "' , '" + history + "')");
		}
	}

	public static String InfosToBanHistory(String banart, String whobanned, String time, String reason) {
		return whobanned + ",," + banart + ",," + time + ",," + reason;
	}

}
