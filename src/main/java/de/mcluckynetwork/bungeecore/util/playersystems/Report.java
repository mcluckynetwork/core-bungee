package de.mcluckynetwork.bungeecore.util.playersystems;

import lombok.Getter;

import java.util.UUID;

/**
 * Created by Robin on 30.12.2015.
 */
@Getter
public class Report {

    private String reporter;
    private String reported;
    private UUID reportedid;
    private String reason;

    public Report(String reporter, UUID reportedid, String reported, String reason) {
        this.reported = reported;
        this.reporter = reporter;
        this.reportedid = reportedid;
        this.reason = reason;
    }

}
