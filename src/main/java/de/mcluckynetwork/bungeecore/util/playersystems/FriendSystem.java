package de.mcluckynetwork.bungeecore.util.playersystems;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FriendSystem {

	public static boolean areFriends(String player, String friend) {
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM friends WHERE player_uuid = '" + UUIDFetcher.getUUID(player) + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				if(friend_uuid.equals(UUIDFetcher.getUUID(friend))) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean askedPlayer(String p, String target) {
		UUID t;
		if(Main.isPlayerOnline(target)) {
			t = ProxyServer.getInstance().getPlayer(target).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(target);
		}
		UUID pl ;
		if(Main.isPlayerOnline(p)) {
			pl = ProxyServer.getInstance().getPlayer(p).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(p);
		}
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + pl + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				if(friend_uuid.equals(t)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + t + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				if(friend_uuid.equals(pl)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean askedPlayerOption2(String p, String target) {
		UUID t;
		if(Main.isPlayerOnline(target)) {
			t = ProxyServer.getInstance().getPlayer(target).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(target);
		}
		UUID pl;
		if(Main.isPlayerOnline(p)) {
			pl = ProxyServer.getInstance().getPlayer(p).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(p);
		}
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT player_uuid FROM waiting_friends WHERE friend_uuid = '" + t + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				if(friend_uuid.equals(pl)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getStatus(String p, String target) {
		UUID t;
		if(Main.isPlayerOnline(target)) {
			t = ProxyServer.getInstance().getPlayer(target).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(target);
		}
		UUID pl;
		if(Main.isPlayerOnline(target)) {
			pl = ProxyServer.getInstance().getPlayer(target).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(p);
		}
		String status = null;
		if(askedPlayer(p, target)) {
			try {
				ResultSet rs = Main.getMySQL().Query("SELECT status FROM waiting_friends WHERE player_uuid = '" + pl + "' AND friend_uuid = '" + t + "'");
				while(rs.next()) {
					status = rs.getString(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}
	
	public static void addFriend(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("INSERT INTO waiting_friends(player_uuid,friend_uuid,status) VALUES ('" + t + "' , '" + pl + "' , '" + 0 + "')");
	}
	
	public static void acceptFriendOffline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM waiting_friends WHERE player_uuid='" + pl + "' AND friend_uuid='" + t + "'");
		Main.getMySQL().Update("INSERT INTO waiting_friends (player_uuid,friend_uuid,status) VALUES ('" + t + "' , '" + pl + "' , '" + 1 + "')");
		Main.getMySQL().Update("INSERT INTO friends(player_uuid,friend_uuid) VALUES ('" + pl + "' , '" + t + "')");
		Main.getMySQL().Update("INSERT INTO friends(player_uuid,friend_uuid) VALUES ('" + t + "' , '" + pl + "')");
	}
	
	public static void acceptFriendOnline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM waiting_friends WHERE player_uuid = '" + pl + "' AND friend_uuid = '" + t + "'");
		Main.getMySQL().Update("INSERT INTO friends(player_uuid,friend_uuid) VALUES ('" + pl + "' , '" + t + "')");
		Main.getMySQL().Update("INSERT INTO friends(player_uuid,friend_uuid) VALUES ('" + t + "' , '" + pl + "')");
	}
	
	public static void denyFriendOffline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM waiting_friends WHERE player_uuid = '" + pl + "' AND friend_uuid = '" + t + "'");
		Main.getMySQL().Update("INSERT INTO waiting_friends (player_uuid,friend_uuid,status) VALUES ('" + t + "' , '" + pl + "' , '" + 2 + "')");
	}
	
	public static void denyFriendOnline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM waiting_friends WHERE player_uuid = '" + t + "' AND friend_uuid = '" + pl + "'");
	}
	
	public static void removeFriendOffline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM friends WHERE player_uuid = '" + pl + "' AND friend_uuid = '" + t + "'");
		Main.getMySQL().Update("DELETE FROM friends WHERE player_uuid = '" + t + "' AND friend_uuid = '" + pl + "'");
		Main.getMySQL().Update("INSERT INTO waiting_friends(player_uuid,friend_uuid,status) VALUES ('" + t + "' , '" + pl + "' , '" + 3 +  "')");
	}
	
	public static void removeFriendOnline(String player, String friend) {
		UUID t;
		if(Main.isPlayerOnline(friend)) {
			t = ProxyServer.getInstance().getPlayer(friend).getUniqueId();
		}else{
			t = UUIDFetcher.getUUID(friend);
		}
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		Main.getMySQL().Update("DELETE FROM friends WHERE player_uuid = '" + pl + "' AND friend_uuid = '" + t + "'");
		Main.getMySQL().Update("DELETE FROM friends WHERE player_uuid = '" + t + "' AND friend_uuid = '" + pl + "'");
	}
	
	public static List<Friend> getFriends(String player) {
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		List<Friend> f = new ArrayList<>();
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM friends WHERE player_uuid = '" + pl + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				String fname = UUIDFetcher.getName(friend_uuid);
				f.add(new Friend(friend_uuid, fname));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return f;
	}
	
	public static List<Friend> getAnfragen(String player) {
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		List<Friend> f = new ArrayList<>();
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + pl + "' AND status = '" + 0 + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				String fname = UUIDFetcher.getName(friend_uuid);
				f.add(new Friend(friend_uuid, fname));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return f;
	}
	
	public static List<Friend> getAccepted(String player) {
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		List<Friend> f = new ArrayList<>();
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + pl + "' AND status = '" + 1 + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				String fname = UUIDFetcher.getName(friend_uuid);
				f.add(new Friend(friend_uuid, fname));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for(Friend id : f) {
			Main.getMySQL().Update("DELETE FROM waiting_friends WHERE friend_uuid='" + id.getUuid() + "' AND player_uuid='" + pl + "'");
		}
		return f;
	}
	
	public static List<Friend> getDenyed(String player) {
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		List<Friend> f = new ArrayList<>();
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + pl + "' AND status = '" + 2 + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				String fname = UUIDFetcher.getName(friend_uuid);
				f.add(new Friend(friend_uuid, fname));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for(Friend id : f) {
			Main.getMySQL().Update("DELETE FROM waiting_friends WHERE friend_uuid='" + id.getUuid() + "' AND player_uuid='" + pl + "'");
		}
		return f;
	}
	
	public static List<Friend> getRemoved(String player) {
		UUID pl;
		if(Main.isPlayerOnline(player)) {
			pl = ProxyServer.getInstance().getPlayer(player).getUniqueId();
		}else{
			pl = UUIDFetcher.getUUID(player);
		}
		List<Friend> f = new ArrayList<>();
		try {
			ResultSet rs = Main.getMySQL().Query("SELECT friend_uuid FROM waiting_friends WHERE player_uuid = '" + pl + "' AND status = '" + 3 + "'");
			while(rs.next()) {
				String id = rs.getString(1);
				UUID friend_uuid = UUID.fromString(id);
				String fname = UUIDFetcher.getName(friend_uuid);
				f.add(new Friend(friend_uuid, fname));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for(Friend id : f) {
			Main.getMySQL().Update("DELETE FROM waiting_friends WHERE friend_uuid='" + id.getUuid() + "' AND player_uuid='" + pl + "'");
		}
		return f;
	}

	@Getter
	public static class Friend{

		private UUID uuid;
		private String name;

		public Friend(UUID uuid, String name) {
			this.uuid = uuid;
			this.name = name;
		}

	}
	
 }
	
	