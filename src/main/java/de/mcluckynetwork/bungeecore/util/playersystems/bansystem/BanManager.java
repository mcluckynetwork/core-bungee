package de.mcluckynetwork.bungeecore.util.playersystems.bansystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 30.12.2015.
 */
public class BanManager {

    private static List<BanProfile> profiles = new ArrayList<>();
    private static List<BanType> types = new ArrayList<>();

    public static void addBanProfile(BanProfile banProfile) {
        profiles.add(banProfile);
    }

    public static void removeBanProfile(BanProfile banProfile) {
        profiles.remove(banProfile);
    }

    public static BanProfile getBanProfile(int id) {
        for(BanProfile profile : profiles) {
            if(profile.getId() == id) {
                return profile;
            }
        }
        return null;
    }

    public static void addBanType(BanType type) {
        types.add(type);
    }

    public static void removeBanType(BanType type) {
        types.remove(type);
    }

    public static BanType getBanType(int id) {
        for(BanType banType : types) {
            if(banType.getId() == id) {
                return banType;
            }
        }
        return null;
    }

    public static List<BanProfile> getBanProfiles() {
        return profiles;
    }

    public static List<BanType> getBanTypes() {
        return types;
    }

    public static int getIntFromBoolean(boolean b) {
        if(b) {
            return 1;
        }
        return 0;
    }

    public static boolean getBooleanFromInt(int i) {
        if(i == 1) {
            return true;
        }
        return false;
    }

}
