package de.mcluckynetwork.bungeecore.util.playersystems.bansystem;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Robin on 30.12.2015.
 */
@Getter
public class BanType {

    private int id;
    private String name;
    @Setter
    private BanProfile banProfile;

    public BanType(int id, String name, BanProfile banProfile) {
        this.id = id;
        this.name = name;
        this.banProfile = banProfile;
    }

}
