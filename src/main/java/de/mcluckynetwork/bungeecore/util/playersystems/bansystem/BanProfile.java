package de.mcluckynetwork.bungeecore.util.playersystems.bansystem;

import lombok.Getter;

/**
 * Created by Robin on 30.12.2015.
 */
@Getter
public class BanProfile {

    private int id;
    private String name;
    private int time;
    private boolean bantype;
    private boolean videoid;

    public BanProfile(int id, String name, int time, boolean bantype, boolean videoid) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.bantype = bantype;
        this.videoid = videoid;
    }
}
