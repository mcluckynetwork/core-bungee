package de.mcluckynetwork.bungeecore.clients;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import lombok.Getter;
import lombok.Setter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Robin on 23.07.2015.
 */

@Getter
public class Client {

    private UUID uuid;
    private String name;
    private Rang rang;
    private int online;
    private List<Setting> settings;
    private char[] rsettings;
    private List<String> reports;
    private int reps;
    @Setter
    private String ret;
    private MuteClient mc;
    private boolean muted;
    @Setter
    private boolean joining;
    @Setter
    private long join;

    public Client(UUID uuid, String name, Rang rang, char[] s, boolean isonline){
        this.uuid = uuid;
        this.rang = rang;
        this.name = name;
        this.reports = new ArrayList<>();
        this.settings = new ArrayList<>();
        this.rsettings = s;
        this.reps = 0;
        for(int i = 0; i < rsettings.length; i++) {
            if(rsettings[i] == 't') {
                settings.add(Setting.fromID(i));
            }
        }
        if(isonline) {
            online = 1;
            this.joining = true;
            if(rang.isTeam()) {
                Main.addClientToTeam(this);
            }
            if(isActiveSetting(Setting.ERROR)) {
                Main.addClientToError(this);
            }
            if(rang.isTeam()) {
                join = System.currentTimeMillis() / 1000;
            }
        }else{
            online = 0;
        }
        this.ret = null;
        MuteClient mc = getMuteClientFromMySQL(uuid);
        if(mc == null) {
            this.muted = false;
        }else{
            this.mc = mc;
            this.muted = true;
        }
    }

    public String getChatName() {
        return getRang().getColor() + getName();
    }

    public boolean isActiveSetting(Setting s) {
        return settings.contains(s);
    }

    public void addReport(String name) {
        this.reports.add(name);
    }

    public boolean hasAlreadyReported(String name) {
        return this.reports.contains(name);
    }

    public void setRang(Rang r) {
        this.rang = r;
        Main.getMySQL().Update("UPDATE clients SET rang='" + r.getId() + "' WHERE uuid='" + uuid + "'");
    }

    public void updateSettings(Setting s, char insert) {
        this.rsettings[s.getId()] = insert;
        if(insert == 'f') {
            this.settings.remove(s);
        }else{
            this.settings.add(s);
        }
        Main.getMySQL().Update("UPDATE clients SET settings='" + new String(rsettings) + "' WHERE uuid='" + uuid + "'");
    }

    public void setMuted(boolean b) {
        this.muted = b;
        if(!b) {
            this.mc = null;
        }
    }

    public void setMuteClient(MuteClient mc) {
        this.mc = mc;
        if(mc != null) {
            this.muted = true;
        }
    }

    public MuteClient getMuteClientFromMySQL(UUID uuid)  {
        try {
            ResultSet rs = Main.getMySQL().Query("SELECT * FROM chatbanusers WHERE uuid = '" + uuid + "'");
            if(rs.next()) {
                String banart = rs.getString("banart");
                String reason = rs.getString("reason");
                int time = rs.getInt("time");
                int value = (int) (time - (System.currentTimeMillis() / 1000));
                if(value > 0) {
                    return new MuteClient(this, banart, reason, time);
                }else{
                    BanSystem.unbanPlayerChatBan(uuid);
                    return null;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
