package de.mcluckynetwork.bungeecore.clients;

import lombok.Getter;

/**
 * Created by Robin on 09.10.2015.
 */
@Getter
public enum Setting {

    NOTIFY(0),

    FRIENDREQUESTS(1),

    PARTYREQUESTS(2),

    ERROR(3),

    DEBUG(4);

    private int id;

    private Setting(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static Setting fromID(int id) {
        for(Setting s : values()) {
            if(s.getId() == id) {
                return s;
            }
        }
        return null;
    }

}
