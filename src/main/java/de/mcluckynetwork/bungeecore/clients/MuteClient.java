package de.mcluckynetwork.bungeecore.clients;

import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Created by Robin on 31.10.2015.
 */
@Getter
public class MuteClient {

    private Client c;
    private String banart;
    private String reason;
    private int time;

    public MuteClient(Client c, String banart, String reason, int time) {
        this.banart = banart;
        this.reason = reason;
        this.time = time;
        this.c = c;
    }

    public void sendMessage(ProxiedPlayer p) {
        int value = (int) (time - (System.currentTimeMillis() / 1000));
        String[] binfos = banart.split(",");
        if(binfos[0].equalsIgnoreCase("chatban")) {
            if(value > 0) {
                int[] zeit = BanSystem.secondsToArrays(value);
                TextComponent tc = new TextComponent("§e" + reason);
                if(reason.contains("[") && reason.contains("]")) {
                    String re = reason.replace("[", ";");
                    String[] rdetails = re.split(";");
                    String ch = rdetails[1];
                    ch = ch.replace("]", "");
                    tc.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://luckyworld.eu/chatlog/" + ch));
                }
                int day = zeit[1];
                int hour = zeit[2];
                int min = zeit[3];
                int sek = zeit[4];
                p.sendMessage(new TextComponent("§7[§cChat§7] §7Du wurdest aus dem Chat gebannt!"));
                if(day == 0 && hour == 0 && min == 0) {
                    p.sendMessage(new TextComponent("§7[§cChat§7] §7Grund: "), tc, new TextComponent(" §7Verbleibende Zeit: §e" + sek + " §7Sekunden"));
                }else if(day == 0 && hour == 0) {
                    p.sendMessage(new TextComponent("§7[§cChat§7] §7Grund: "), tc, new TextComponent(" §7Verbleibende Zeit: §e" + min + " §7Minuten §e" + sek + " §7Sekunden"));
                }else if(day == 0) {
                    p.sendMessage(new TextComponent("§7[§cChat§7] §7Grund: "), tc, new TextComponent(" §7Verbleibende Zeit: §e" + hour + " §7Stunden §e" + min + " §7Minuten §e" + sek + " §7Sekunden"));
                }else {
                    p.sendMessage(new TextComponent("§7[§cChat§7] §7Grund: "), tc, new TextComponent(" §7Verbleibende Zeit: §e" + day + " §7Tage §e" + hour + " §7Stunden §e" + min + " §7Minuten §e" + sek + " §7Sekunden"));
                }
            }else{
                BanSystem.unbanPlayerChatBan(p.getUniqueId());
            }
        }
    }

}
