package de.mcluckynetwork.bungeecore.clients;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;

/**
 * Created by Robin on 14.01.2016.
 */
@Getter
public enum Rang {

    SPIELER(0, "§2Spieler", ChatColor.DARK_GREEN, false, "Spieler"),

    PREMIUM(1, "§6Premium", ChatColor.GOLD, false, "Premium"),

    PREMIUMPLUS(2, "§6PremiumPlus", ChatColor.GOLD, false, "PremiumPlus"),

    YOUTUBER(3, "§5YouTuber", ChatColor.DARK_PURPLE, false, "YouTuber"),

    ARCHITEKT(4, "§aArchitekt", ChatColor.GREEN, true, "Architekt"),

    TESTMODERATOR(5, "§cModerator", ChatColor.RED, true, "Testmoderator"),

    MODERATOR(6, "§cModerator", ChatColor.RED, true, "Moderator"),

    SENIORMODERATOR(7, "§cSrModerator", ChatColor.RED, true, "SrModerator"),

    DEVELOPER(8, "§bDeveloper", ChatColor.AQUA, true, "Developer"),

    ADMINSTRATOR(9, "§4Admin", ChatColor.DARK_RED, true, "Admin");

    private int id;
    private String name;
    private ChatColor color;
    private boolean team;
    private String rawName;

    Rang(int id, String name, ChatColor color, boolean team, String rname) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.team = team;
        this.rawName = rname;
    }

    public static Rang fromID(int id) {
        for (Rang r : values()) {
            if (r.getId() == id) {
                return r;
            }
        }
        return SPIELER;
    }

    public static Rang fromName(String name) {
        for(Rang r : values()) {
            if(r.getRawName().equalsIgnoreCase(name)) {
                return r;
            }
        }
        return null;
    }
}

