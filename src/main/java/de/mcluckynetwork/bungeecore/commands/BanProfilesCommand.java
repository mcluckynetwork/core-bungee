package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanManager;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanProfile;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Robin on 30.12.2015.
 */
public class BanProfilesCommand extends Command {

    public BanProfilesCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(!(c.getRang().getId() >= Rang.TESTMODERATOR.getId() && c.getRang() != Rang.DEVELOPER)) {
                return;
            }
            if(args.length == 0) {
                sendProfilesList(p);
            }else if(args.length == 2) {
                if(args[0].equalsIgnoreCase("remove")) {
                    if(c.getRang() != Rang.ADMINSTRATOR) {
                        return;
                    }
                    try {
                        int id = Integer.valueOf(args[1]);
                        try {
                            ResultSet rs = Main.getMySQL().Query("SELECT * FROM banprofiles WHERE id='" + id + "'");
                            if (rs.next()) {
                                Main.getMySQL().Update("DELETE FROM banprofiles WHERE id='" + id + "'");
                                BanProfile banProfile = BanManager.getBanProfile(id);
                                if(banProfile != null) {
                                    BanManager.removeBanProfile(banProfile);
                                }
                                p.sendMessage(new TextComponent("§7[§4System§7] §7Du §7hast §7das §7Profil §e" + id + " §7erfolgreich §7entfernt"));
                            }else{
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Profil §e " + id + " §7konnte §7nicht §7gefunden §7werden"));
                            }
                        }catch(SQLException ex) {
                            ex.printStackTrace();
                        }
                    }catch(NumberFormatException ex) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7eine §7gültige §7Zahl §7ein"));
                    }
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
                }
            }else if(args.length == 6) {
                if (args[0].equalsIgnoreCase("add")) {
                    if(c.getRang() != Rang.ADMINSTRATOR) {
                        return;
                    }
                    try{
                        int id = Integer.valueOf(args[1]);
                        String name = args[2];
                        int time = Integer.valueOf(args[3]);
                        String bantype = args[4];
                        boolean bt = false;
                        if(bantype.equalsIgnoreCase("true")) {
                            bt = true;
                        }
                        String vidid = args[5];
                        boolean videoid = false;
                        if(vidid.equalsIgnoreCase("true")) {
                            videoid = true;
                        }
                        try {
                            ResultSet rs = Main.getMySQL().Query("SELECT * FROM banprofiles WHERE id='" + id + "'");
                            if(rs.next()) {
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Profil §7existiert §7schon"));
                            }else{
                                name = name.replace("_", " ");
                                Main.getMySQL().Update("INSERT INTO banprofiles(id, name, time, bantype, videoid) VALUES('" + id + "' , '" + name + "' , '" + time + "' , '" + BanManager.getIntFromBoolean(bt) + "' , '" + BanManager.getIntFromBoolean(videoid) + "')");
                                BanProfile banProfile = BanManager.getBanProfile(id);
                                for(BanType type : BanManager.getBanTypes()) {
                                    if(type.getBanProfile().getId() == id) {
                                        type.setBanProfile(banProfile);
                                        p.sendMessage(new TextComponent("§7[§4System§4] §7Ban-Profil §7des §7Types §e" + type.getId() + " §7wurde §7überschrieben"));
                                    }
                                }
                                if(banProfile != null) {
                                    BanManager.removeBanProfile(banProfile);
                                }
                                BanManager.addBanProfile(new BanProfile(id, name, time, bt, videoid));
                                p.sendMessage(new TextComponent("§7[§4System§7] §7Das §7Profil §e" + id + " §7wurde §7erfolgreich §7hinzugefügt"));
                            }
                        }catch(SQLException ex) {
                            ex.printStackTrace();
                        }
                    }catch(NumberFormatException ex) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7eine §7gültige §7Zahl §7ein"));
                    }
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
            }
        }
    }

    public void sendProfilesList(ProxiedPlayer proxiedPlayer) {
        proxiedPlayer.sendMessage(new TextComponent("§7[§4System§7] §7Ban-Profile:"));
        BanManager.getBanProfiles().forEach(banprofile -> {
            String time;
            if(banprofile.getTime() == 0) {
                time = "Permanent";
            }else{
                time = banprofile.getTime() + " §eday";
            }
            String bantype = "§eNein";
            String videoid = "§eNein";
            if(banprofile.isBantype()) {
                bantype = "§eJa";
            }
            if(banprofile.isVideoid()) {
                videoid = "§eJa";
            }
            proxiedPlayer.sendMessage(new TextComponent("§7- ID: §e" + banprofile.getId() + " §7| §7Name: §e" + banprofile.getName() + " §7| §7Zeit: §e" + time + " §7| §7BanTyp: " + bantype + " §7| Video-ID: " + videoid));
        });
    }

}
