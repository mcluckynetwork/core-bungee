package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketTeamMessage;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamChatCommand extends Command{

	public TeamChatCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p.getName());
			if(!c.getRang().isTeam()) {
				return;
			}
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < args.length; i++) {
				str.append(args[i] + " §7");
			}
			String message = str.toString();
			Main.sendTeamMessage(c.getChatName(), "§7" + message);
			ChatClient.sendPacket(new PacketTeamMessage(Main.name, c.getName(), message));
		}else{
			sender.sendMessage(new TextComponent("Nur für Spieler"));
		}
	}

}
