package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 03.10.2015.
 */
public class WartungCommand extends Command {
    
    public WartungCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (c.getRang().getId() < Rang.SENIORMODERATOR.getId()) {
                return;
            }
            if (args.length == 2) {
                if (args[0].equalsIgnoreCase("add")) {
                    String name = args[1];
                    UUID uuid = UUIDFetcher.getUUID(name);
                    Client t = Main.getOfflineClient(uuid, name);
                    if (uuid == null) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                    } else {
                        if (!Main.wartungen.contains(uuid)) {
                            Main.wartungen.add(uuid);
                            Main.sendTeamMessage("System", t.getChatName() + " §7wurde zu den Wartungsarbeiten hinzugefügt");
                        } else {
                            p.sendMessage(new TextComponent("§7[§aHilfe§7] " + t.getChatName() + " ist bereits in den Wartungsarbeiten"));
                        }
                    }
                } else if (args[0].equalsIgnoreCase("remove")) {
                    String name = args[1];
                    UUID uuid = UUIDFetcher.getUUID(name);
                    Client t = Main.getOfflineClient(uuid, name);
                    if (uuid == null) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                    } else {
                        if (Main.wartungen.contains(uuid)) {
                            Main.wartungen.remove(uuid);
                            Main.sendTeamMessage("System", t.getChatName() + " §7wurde von den Wartungsarbeiten entfernt");
                        } else {
                            p.sendMessage(new TextComponent("§7[§aHilfe§7] " + t.getChatName() + " ist nicht in den Wartungsarbeiten"));
                        }
                    }
                } else {
                    sendHelpMessage(p);
                }
            } else {
                sendHelpMessage(p);
            }
        }
    }

    private void sendHelpMessage(ProxiedPlayer p) {
        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Wartungen Hilfe:"));
        p.sendMessage(new TextComponent("§e/wartung add <Player> §7Der §7Spieler §7kann §7dann §7den§7 Server §7in §7Wartungsarbeiten §7betreten"));
        p.sendMessage(new TextComponent("§e/wartung remove <Player> §7Der §7Spieler §7kann §7dann §7den §7Server §7nicht §7mehr §7in §7Wartungsarbeiten §7betreten"));
    }

}
