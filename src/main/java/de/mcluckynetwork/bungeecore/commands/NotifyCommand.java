package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Setting;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 09.10.2015.
 */
public class NotifyCommand extends Command {

    public NotifyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (!c.getRang().isTeam()) {
                return;
            }
            if (c.isActiveSetting(Setting.NOTIFY)) {
                Main.team.remove(c);
                c.updateSettings(Setting.NOTIFY, 'f');
                p.sendMessage(new TextComponent("§7[§4System§7] §7Du §7bekommst §7nun §7keine §7Teamnachrichten §7mehr"));
            } else {
                Main.team.add(c);
                c.updateSettings(Setting.NOTIFY, 't');
                p.sendMessage(new TextComponent("§7[§4System§7] §7Du §7bekommst §7nun §7wieder §7Teamnachrichten"));
            }
        } else {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}