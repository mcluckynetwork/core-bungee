package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.MojangIDFetcher;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 14.01.2016.
 */

public class AddUUIDCommand extends Command{

    public AddUUIDCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 1) {
                String name = args[0];
                UUID uuid = UUIDFetcher.getUUID(name);
                if(uuid != null) {
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Diese UUID ist bereits eingetragen"));
                }else{
                    UUID mojangid = MojangIDFetcher.getUUID(name);
                    Main.getMySQL().Update("INSERT INTO uuids(uuid, name) VALUES('" + mojangid + "' , '" + name + "')");
                    p.sendMessage(new TextComponent(Main.pre + "§7Die UUID von §e" + name + "(§e" + mojangid + "§7) §7wurde eingetragen"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /adduuid <Name>"));
            }
        }else{
            sender.sendMessage(new TextComponent("Momentan nur für Spieler"));
        }
    }
}
