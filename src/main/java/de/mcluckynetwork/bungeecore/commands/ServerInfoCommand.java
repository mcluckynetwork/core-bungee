package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketServerInfo;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 01.01.2016.
 */
public class ServerInfoCommand extends Command{

    public ServerInfoCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("rl")) {
                    ProxyServer.getInstance().getServers().clear();
                    Main.setupServer();
                    return;
                }
                String name = args[0];
                if(Main.isServerInMap(name)) {
                    ServerInfo info = ProxyServer.getInstance().getServers().get(name);
                    p.sendMessage(new TextComponent(Main.pre + "§7Informationen zum Server: §e" + name));
                    p.sendMessage(new TextComponent(Main.pre + "§7IP: " + info.getAddress().getHostString()));
                    p.sendMessage(new TextComponent(Main.pre + "§7Port: " + info.getAddress().getPort()));
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Der Server §e" + name + " §7existiert nicht"));
                }
            }else if(args.length == 2) {
                if(args[0].equalsIgnoreCase("remove")) {
                    String name = args[1];
                    if(Main.isServerInMySQL(name)) {
                        Main.removeServer(name);
                        p.sendMessage(new TextComponent("§7[§4System§7] §7Der Server §e" + name + " §7wurde erfolgreich gelöscht"));
                        ChatClient.sendPacket(new PacketServerInfo(Main.name, name, "Host", 123, PacketServerInfo.ServerAction.REMOVE));
                    }else{
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieser Server existiert nicht"));
                    }
                }
            }else if(args.length == 4) {
                if(args[0].equalsIgnoreCase("add")) {
                    try {
                        String name = args[1];
                        String host = args[2];
                        int port = Integer.valueOf(args[3]);
                        if(Main.isServerInMySQL(name)) {
                            p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Server existiert bereits"));
                        }else{
                            Main.addServer(name, host, port);
                            ChatClient.sendPacket(new PacketServerInfo(Main.name, name, host, port, PacketServerInfo.ServerAction.ADD));
                            p.sendMessage(new TextComponent("§7[§4System§7] §7Der §7Server §e" + name + " §7wurde hinzugefügt"));
                        }
                    }catch(NumberFormatException ex) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7einen §7gültigen §7Port §7an"));
                    }
                }
            }
        }
    }

}
