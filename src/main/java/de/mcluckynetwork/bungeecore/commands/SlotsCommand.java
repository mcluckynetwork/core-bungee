package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketSlotUpdate;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 04.12.2015.
 */
public class SlotsCommand extends Command {

    public SlotsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 0) {
                p.sendMessage(new TextComponent(Main.pre + "§7Aktuelle Slotanzahl: §e" + Main.maximal));
            }else if(args.length == 1 ) {
                int slots = 0;
                try{
                   slots = Integer.valueOf(args[0]);
                }catch(NumberFormatException e) {
                    p.sendMessage(new TextComponent(Main.pre + "§7Dies ist keine gültige Zahl!"));
                }
                Main.maximal = slots;
                PacketSlotUpdate packet = new PacketSlotUpdate(Main.name, slots);
                ChatClient.sendPacket(packet);
                Main.sendTeamMessage("System", "§7Die Slotanzahl wurde auf §e" + slots + " §7erhöht");
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/slots §7oder §7/slots §7<Anzahl>"));
            }
        }else{
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}
