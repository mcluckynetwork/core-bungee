package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StatusCommand extends Command{

	public StatusCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(sender.getName());
			if (!c.getRang().isTeam()) {
				return;
			}
			if (args.length == 1) {
				ProxyState state = ProxyState.getProxyStatefromState(args[0]);
				if(state == null) {
					p.sendMessage(new TextComponent(Main.pre + "§7Dieser §7Status §7ist §7nicht §7verfügbar"));
					return;
				}
				Main.state = state;
				Main.sendTeamMessage("System", "§7" + Main.name + " §7Neuer §7Status: §e" + Main.state.getState().toUpperCase());
				if(Main.state == ProxyState.MAINTENANCE) {
					for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						Client a = Main.getClient(all);
						if (!a.getRang().isTeam()) {
							all.disconnect(new TextComponent(Main.pre + "§7Du kannst das Netzwerk momentan nicht betreteten \n§cGrund: §4WARTUNGSARBEITEN"));
						}
					}
					Main.wartungen.add(p.getUniqueId());
				}
			} else {
				p.sendMessage(new TextComponent(Main.pre + "§7Nutze §7/status §eStatus"));
			}
		}
	}

}
