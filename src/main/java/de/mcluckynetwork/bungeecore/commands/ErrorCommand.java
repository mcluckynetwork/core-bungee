package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.clients.Setting;
import de.mcluckynetwork.bungeecore.util.exception.ExceptionManager;
import de.mcluckynetwork.bungeecore.util.exception.PluginException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 11.10.2015.
 */
public class ErrorCommand extends Command {

    public ErrorCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang().getId() < Rang.DEVELOPER.getId()) {
                return;
            }
            if(args.length == 0) {
                if(Main.error.contains(c)) {
                    c.updateSettings(Setting.ERROR, 'f');
                    Main.error.remove(c);
                    p.sendMessage(new TextComponent(Main.pre + "§7Du §7erhältst §7nun §7keine §7Error §7Messages §7mehr"));
                }else{
                    c.updateSettings(Setting.ERROR, 't');
                    Main.addClientToError(c);
                    p.sendMessage(new TextComponent(Main.pre + "§7Du §7erhältst §7nun  §7Error §7Messages"));
                }
            }else if(args.length == 1) {
                String id = args[0];
                UUID uuid = UUID.fromString(id);
                if(ExceptionManager.isException(uuid)) {
                    PluginException ex = ExceptionManager.getException(uuid);
                    p.sendMessage(new TextComponent(Main.pre + ChatColor.GRAY + "Exception-Infos " + ChatColor.YELLOW + uuid));
                    p.sendMessage(new TextComponent(Main.pre + ChatColor.GRAY + "Typ: " + ChatColor.YELLOW + ex.getType()));
                    for(StackTraceElement e : ex.getElements()) {
                        p.sendMessage(new TextComponent(e.toString()));
                    }
                    StringBuilder str = new StringBuilder();
                    for(int i = 0; i < ex.getDebug().size(); i++) {
                        String name = ex.getDebug().get(i);
                        if(i == ex.getDebug().size()) {
                            str.append(name);
                        }else{
                            str.append(name + ", ");
                        }
                    }
                    p.sendMessage(new TextComponent("§7Debugs: §e" + str.toString()));
                }else{
                    p.sendMessage(new TextComponent(Main.pre + "§7Dieser Exception-Code existiert nicht"));
                }
            }
        }
    }

}
