package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.text.NumberFormat;

/**
 * Created by Robin on 18.12.2015.
 */
public class BelastendCommand extends Command {

    public BelastendCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            Runtime runtime = Runtime.getRuntime();
            NumberFormat format = NumberFormat.getInstance();

            long maxMemory = runtime.maxMemory();
            long freeMemory = runtime.freeMemory();
            long allocatedMemory = runtime.totalMemory();
            String free = format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024);

            p.sendMessage(new TextComponent(Main.pre + "§7Maximaler Ram: §e" + format.format(maxMemory / 1024)));
            p.sendMessage(new TextComponent(Main.pre + "§7Freier Ram: §e" + format.format(freeMemory / 1024)));
            p.sendMessage(new TextComponent(Main.pre + "§7Zugeordneter Ram: §e" + format.format(allocatedMemory / 1024)));
            p.sendMessage(new TextComponent(Main.pre + "§7Insgesamt freier Ram: §e" + free));
        }else{
            sender.sendMessage(new TextComponent("Mach doch top oder free #Lappen..."));
        }
    }

}
