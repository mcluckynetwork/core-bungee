package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketBroadcastMessage;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 24.07.2015.
 */
public class BroadcastCommand extends Command{

    public BroadcastCommand(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p.getName());
            if (c.getRang().getId() >= Rang.SENIORMODERATOR.getId()) {
                if (args.length == 0) {
                    sender.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /bc <Nachricht>"));
                    return;
                }
                String latest = "§7";
                TextComponent component = new TextComponent("§7[§4WICHTIG§7] ");
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < args.length; i++) {
                    String raw;
                    if (args[i].contains("&")) {
                        latest = "§" + args[i].charAt(1);
                        String raw1 = args[i].replace("&" + args[i].charAt(1), "");
                        raw = latest + raw1 + " ";
                    } else {
                        raw = latest + args[i] + " ";
                    }
                    str.append(raw);
                    TextComponent l = new TextComponent(raw);
                    component.addExtra(l);
                }
                ProxyServer.getInstance().broadcast(component);
                ChatClient.sendPacket(new PacketBroadcastMessage(Main.name, str.toString()));
            }
        }
    }

}
