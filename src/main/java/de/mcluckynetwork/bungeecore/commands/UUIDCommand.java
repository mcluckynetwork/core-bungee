package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class UUIDCommand extends Command{

	public UUIDCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if (!c.getRang().isTeam()) {
				return;
			}
			if (args.length == 0) {
				p.sendMessage(new TextComponent(Main.pre + "§7Deine §7UUID: §e" + UUIDFetcher.getUUID(p.getName())));
			} else if (args.length == 1) {
				UUID uuid = UUIDFetcher.getUUID(args[0]);
				if (uuid != null) {
					p.sendMessage(new TextComponent(Main.pre + "§7UUID §7von " + Main.getOnOfflineClient(uuid, args[0]) + "§7: " + UUIDFetcher.getUUID(args[0])));
				} else {
					p.sendMessage(new TextComponent(Main.pre + "§7UUID §7des §7Spielers §2" + args[0] + " §7konnte §7nicht §7gefunden §7werden"));
				}
			}
		} else {
			sender.sendMessage(new TextComponent("Nur für Spieler"));
		}
	}

}
