package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 29.08.2015.
 */
public class PartyCommand extends Command {

    public PartyCommand(String name)  {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            p.sendMessage(new TextComponent(Main.pre + "§7Dieses System ist derzeit im §cWartungsmodus!"));
        } else {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }
}