package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class BanLogCommand extends Command{

	public BanLogCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {	
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if (c.getRang().getId() < 5) {
				return;
			}
			if (args.length != 1) {
				p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/banlog §7<Spieler>"));
				return;
			}
			String name = args[0];
			Client targetclient;
			UUID uuid;
			ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
			if (target != null) {
				uuid = target.getUniqueId();
				targetclient = Main.getClient(target.getName());
			} else {
				uuid = UUIDFetcher.getUUID(name);
				if (uuid != null) {
					targetclient = Main.getOfflineClient(uuid, name);
				} else {
					p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
					return;
				}
			}
			if (targetclient != null) {
				p.sendMessage(new TextComponent("§7[§4System§7] §7Banlog von " + targetclient.getChatName() + "§7:"));
				if (BanSystem.isInBanTable(uuid)) {
					String[] activeBan = BanSystem.getAktivBanInfos(uuid);
					int timest = Integer.valueOf(activeBan[3]);
					Timestamp stamp = new Timestamp(timest);
					Date date = new Date(stamp.getTime());
					String[] infos = activeBan[1].split(",");
					String type = infos[0];
					String time = infos[1];
					String t = activeBan[0];
					UUID id = UUID.fromString(t);
					Client ban = Main.getOfflineClient(id, UUIDFetcher.getName(id));
					if(ban != null) {
						p.sendMessage(new TextComponent("§e" + type + " §7|§e " + time + " §7|§e " + activeBan[2] + " §7|§e " + ban.getChatName() + " §7| §e " + date.getDay() + " " + date.getMonth() + " " + date.getYear()));
					}
				} else {
					p.sendMessage(new TextComponent("§7Kein §7aktiver §7Ban"));
				}
				if (BanSystem.isInChatBanTable(uuid)) {
					String[] activeBan = BanSystem.getAktivChatBanInfos(uuid);
					String[] infos = activeBan[1].split(",");
					String type = infos[0];
					String time = infos[1];
					String t = activeBan[0];
					UUID id = UUID.fromString(t);
					Client ban = Main.getOfflineClient(id, UUIDFetcher.getName(id));
					if(ban != null) {
						p.sendMessage(new TextComponent("§e" + type + " §7|§e " + time + " §7|§e " + activeBan[2] + " §7|§e " + ban.getChatName()));
					}else{
						p.sendMessage(new TextComponent("§e" + type + " §7|§e " + time + " §7|§e " + activeBan[2]));
					}
				} else {
					p.sendMessage(new TextComponent("§7Kein §7aktiver §7ChatBan"));
				}
				p.sendMessage(new TextComponent(" "));
				if (BanSystem.isInHistoryTable(uuid)) {
					String bans = BanSystem.getBanHistory(uuid);
					if(bans != null) {
						String[] ebans = bans.split("#");
						for(int i = 0; i < ebans.length; i++) {
							String[] infos = ebans[i].split(",,");
							String whoBanned = infos[0];
							String banType = infos[1];
							String time = infos[2];
							String reason = infos[3];
							UUID tuuid = UUID.fromString(whoBanned);
							Client ban = Main.getOfflineClient(tuuid, UUIDFetcher.getName(tuuid));
							p.sendMessage(new TextComponent("§e" + banType + " §7|§e " + time + " §7|§e " + reason + " §7|§e " + ban.getChatName()));
						}
					}else{
						p.sendMessage(new TextComponent("§cFehler bei der Ban-Verarbeitung"));
					}
				} else {
					p.sendMessage(new TextComponent("§7Keine §7vergangenen §7Bans"));
				}
			}else {
				p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7abgerufen §7werden."));
			}
		}
	}

}
