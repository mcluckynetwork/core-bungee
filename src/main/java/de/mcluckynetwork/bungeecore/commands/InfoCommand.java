package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 28.10.2015.
 */
public class InfoCommand extends Command {

    public InfoCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(!c.getRang().isTeam()) {
                return;
            }
            p.sendMessage(new TextComponent(Main.pre + "§7Netzwerk Informationen"));
            p.sendMessage(new TextComponent("§7Spieler online: §e" + Main.online));
        }else{
            sender.sendMessage(new TextComponent("Du wirst wohl genug Infos ham...."));
        }
    }

}
