package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketFriendMsg;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 18.10.2015.
 */
public class RCommand extends Command{

    public RCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (args.length >= 1) {
                if (c.getRet() == null) {
                    p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7bisher §7keine §7Nachrichten §7erhalten"));
                    return;
                }
                if(Main.isPlayerOnProxy(c.getRet())) {
                    if(Main.isPlayerOnline(c.getRet())) {
                        if(FriendSystem.areFriends(p.getName(), c.getRet())) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < args.length; i++) {
                                builder.append(args[i] + " §7");
                            }
                            String msg = builder.toString();
                            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(c.getRet());
                            Client t = Main.getClient(target);
                            t.setRet(p.getName());
                            target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
                            p.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
                        }else {
                            Client t = Main.getClient(c.getRet());
                            p.sendMessage(new TextComponent(Main.fpre + "§cDu bist nicht mit " + t.getChatName() + " §cabefreundet!"));
                        }
                    }else{
                        if(FriendSystem.areFriends(p.getName(), c.getRet())) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < args.length; i++) {
                                builder.append(args[i] + " §7");
                            }
                            String msg = builder.toString();
                            Client t = Main.getOfflineClient(UUIDFetcher.getUUID(c.getRet()), c.getRet());
                            p.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
                            ChatClient.sendPacket(new PacketFriendMsg(Main.name, c.getChatName(), c.getRet(), msg));
                        }else{
                            Client t = Main.getClient(c.getRet());
                            p.sendMessage(new TextComponent(Main.fpre + "§cDu bist nicht mit " + t.getChatName() + " §cabefreundet!"));
                        }
                    }
                }else{
                    UUID uuid = UUIDFetcher.getUUID(c.getRet());
                    if (uuid == null) {
                        p.sendMessage(new TextComponent(Main.fpre + "§cUUID §ckonnte §cnicht §cgefunden §cwerden"));
                        return;
                    } else {
                        Client t = Main.getOfflineClient(uuid, c.getRet());
                        p.sendMessage(new TextComponent(Main.fpre + "§cDer Spieler " + t.getChatName() + " §cist offline!"));
                    }
                }
            }else{
                p.sendMessage(new TextComponent(Main.fpre + "§7Nutze §7/r §7<Nachricht>"));
            }
        }else{
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}
