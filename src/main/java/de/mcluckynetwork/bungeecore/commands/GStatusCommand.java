package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketGlobalState;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 31.12.2015.
 */
public class GStatusCommand extends Command {

    public GStatusCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 1) {
                ProxyState state = ProxyState.getProxyStatefromState(args[0]);
                if(state == null) {
                    p.sendMessage(new TextComponent(Main.pre + "§7Dieser §7Status §7ist §7nicht §7verfügbar"));
                    return;
                }
                Main.state = state;
                Main.getMySQL().Update("UPDATE manager SET second='" + Main.state.getId() + "' WHERE first='state'");
                Main.sendTeamMessage("System", "§7Neuer §7Server-Status: §e" + Main.state.getState().toUpperCase());
                if(Main.state == ProxyState.MAINTENANCE) {
                    for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                        Client a = Main.getClient(all);
                        if (!a.getRang().isTeam()) {
                            all.disconnect(new TextComponent(Main.pre + "§7Du kannst das Netzwerk momentan nicht betreteten \n§cGrund: §4WARTUNGSARBEITEN"));
                        }
                    }
                    Main.wartungen.add(p.getUniqueId());
                }
                ChatClient.sendPacket(new PacketGlobalState(Main.name, Main.state));
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutzer §/gstatus §7<Status>"));
            }
        }
    }

}
