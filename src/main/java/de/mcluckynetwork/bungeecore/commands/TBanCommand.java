package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerAction;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanManager;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanProfile;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 14.09.2015.
 */

public class TBanCommand extends Command {

    public TBanCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(!(c.getRang().getId() >= Rang.TESTMODERATOR.getId() && c.getRang() != Rang.DEVELOPER)) {
                return;
            }
            if(args.length >= 2) {
                String name = args[0];
                UUID uuid;
                ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
                if(player != null) {
                    uuid = player.getUniqueId();
                }else{
                    uuid = UUIDFetcher.getUUID(name);
                    if(uuid == null) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                        return;
                    }
                }
                Client t = Main.getOnOfflineClient(uuid, name);
                if(c.getRang() == Rang.TESTMODERATOR || c.getRang() == Rang.MODERATOR) {
                    if(t.getRang().getId() > Rang.PREMIUMPLUS.getId()) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Du §7hast §7nicht §7genügend §7Rechte §7um §7diesen §7Spieler §7zu §7bannen"));
                        return;
                    }
                }else if(c.getRang() == Rang.SENIORMODERATOR) {
                    if(t.getRang().getId() > Rang.YOUTUBER.getId()) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Du §7hast §7nicht §7genügend §7Rechte §7um §7diesen §7Spieler §7zu §7bannen"));
                        return;
                    }
                }
                try{
                    BanProfile profile = BanManager.getBanProfile(Integer.valueOf(args[1]));
                    if(profile != null) {
                        if (profile.isBantype()) {
                            BanType banType = BanManager.getBanType(Integer.valueOf(args[2]));
                            if(banType != null) {
                                if (args.length == 3) {
                                    if (c.getRang() != Rang.TESTMODERATOR) {
                                        banUser(name, uuid, t, p, c, profile, banType, null);
                                    } else {
                                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Du §7musst §7mit §7diesem §7Rang §7eine §7Video-ID §7angeben!"));
                                    }
                                } else {
                                    String videoid = args[3];
                                    banUser(name, uuid, t, p, c, profile, banType, videoid);
                                }
                            }else{
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Der §7Ban-Type §e" + args[2] + " §7existiert §7nicht"));
                            }
                        } else {
                            if (args.length == 2) {
                                banUser(name, uuid, t, p, c, profile, null, null);
                            } else {
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/tban §7<Name> §7<BanProfile>"));
                            }
                        }
                    }else{
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Das §7Ban-Profil §e" + args[1] + " §7existiert §7nicht"));
                    }
                }catch(NumberFormatException ex) {
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7eine §7gültige §7Zahl §7an"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /tban <Name> <Ban-Profil> <Ban-Typ> (<Video-ID>)"));
            }
        }
    }

    public void banUser(String name, UUID uuid, Client t, ProxiedPlayer banner, Client c, BanProfile profile, BanType type, String videoid) {
        int days = 0;
        int time = (int) ((days * 86400) + (System.currentTimeMillis() / 1000));
        String banmessage = profile.getName();
        if(type != null) {
            banmessage = banmessage + "[" + type.getName() + "]";
        }
        if(profile.isVideoid()) {
            if (videoid != null) {
                banmessage = banmessage + "[" + videoid + "]";
            } else {
                banmessage = banmessage + "[Live]";
            }
        }
        BanSystem.manualBanPlayerBan(banner.getUniqueId(), uuid, 0, banmessage, "Permanent");
        if(Main.isPlayerOnProxy(name)) {
            if (Main.isPlayerOnline(name)) {
                int timez = days * 86400;
                int[] zeit = BanSystem.secondsToArrays(timez);
                ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(name);
                if(days != 0) {
                    tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + banmessage + "\n \n §7Verbleibende Zeit: §e" + zeit[0] + " §7Monate §e" + zeit[1] + " §7Tage §e" + zeit[2] + " §7Stunden §e" + zeit[3] + " §7Minuten §e" + zeit[4] + " §7Sekunden \n§7Entbannungsantrag: http://LuckyWorld.eu"));
                }else{
                    tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + banmessage + "\n \n §7 Zeit: §4PERMANENT \n§7Entbannungsantrag: http://LuckyWorld.eu"));
                }
            }else{
                ChatClient.sendPacket(new PacketPlayerAction(Main.name, name, banmessage, PacketPlayerAction.PlayerAction.BAN));
            }
        }
        Main.sendTeamMessage("System", c.getChatName() + " §7hat " + t.getChatName() + " §7gebannt!");
        Main.sendTeamMessage("System", "§7Zeit: " + "§cPermanent " + "§7| Grund: §e" + banmessage);
    }

}

