package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketServerRequest;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 15.09.2015.
 */
public class WhereIsCommand extends Command {

    public WhereIsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (!c.getRang().isTeam()) {
                return;
            }
            if (args.length == 1) {
                String target = args[0];
                if(Main.isPlayerOnProxy(target)) {
                    if (Main.isPlayerOnline(target)) {
                        ProxiedPlayer t = ProxyServer.getInstance().getPlayer(target);
                        TextComponent server = new TextComponent("§e" + t.getServer().getInfo().getName());
                        server.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + t.getServer().getInfo().getName()));
                        p.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler " + Main.getClient(t).getChatName() + " §7ist §7auf §7dem §7Server "), server);
                    } else {
                        ChatClient.sendPacket(new PacketServerRequest(Main.name, target, "Unknown", PacketServerRequest.ServerAction.WHEREIS, p.getName()));
                    }
                }else{
                    UUID uuid = UUIDFetcher.getUUID(target);
                    if (uuid == null) {
                        p.sendMessage(new TextComponent(Main.pre + "§7UUID konnte nicht gefunden werden"));
                        return;
                    }
                    p.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler " + Main.getOfflineClient(uuid, target).getChatName() + " §7ist §coffline"));
                }
            } else {
                p.sendMessage(new TextComponent(Main.pre + "§7Nutze /whereis <Name>"));
            }
        } else {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}
