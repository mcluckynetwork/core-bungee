package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.MuteClient;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerAction;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class MMuteCommand extends Command{

	public MMuteCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p.getName());
			if (!c.getRang().isTeam() && (!(c.getRang() == Rang.ARCHITEKT || c.getRang() == Rang.DEVELOPER))) {
				return;
			}
			if (args.length >= 4) {
				String name = args[0];
				Client tc;
				UUID uuid;
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
				if (target != null) {
					uuid = target.getUniqueId();
					tc = Main.getClient(target.getName());
				} else {
					uuid = UUIDFetcher.getUUID(name);
					if (uuid != null) {
						tc = Main.getOfflineClient(uuid, name);
					} else {
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
						return;
					}
				}
				if (c.getRang() == Rang.TESTMODERATOR || c.getRang() == Rang.MODERATOR) {
					if (tc.getRang().getId() >= 3) {
						sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
						return;
					}
				} else if (c.getRang() == Rang.SENIORMODERATOR) {
					if (tc.getRang().getId() >= 4) {
						sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
						return;
					}
				}
				String t = args[1];
				String rtime = args[2];
				StringBuilder str = new StringBuilder();
				for (int i = 3; i < args.length; i++) {
					str.append(args[i] + " ");
				}
				String reason = str.toString();
				if (rtime.equalsIgnoreCase("day")) {
					int roht = Integer.valueOf(t);
					int time = (int) ((roht * 86400) + (System.currentTimeMillis() / 1000));
					BanSystem.manualChatBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
					Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gemutet!");
					Main.sendTeamMessage("System", "§7Grund: §e " + reason + " §7| §7Zeit:§e " + roht + " §7Tage");
					if(Main.isPlayerOnProxy(tc.getName())) {
						if (Main.isPlayerOnline(tc.getName())) {
							tc.setMuteClient(new MuteClient(c, "ChatBan," + t, reason, time));
						}else{
							ChatClient.sendPacket(new PacketPlayerAction(Main.name, tc.getName(), reason, PacketPlayerAction.PlayerAction.MUTE));
						}
					}
				} else if (rtime.equalsIgnoreCase("hour")) {
					int roht = Integer.valueOf(t);
					int time = (int) ((roht * 3600) + (System.currentTimeMillis() / 1000));
					BanSystem.manualChatBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
					Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gemutet!");
					Main.sendTeamMessage("System", "§7Grund: §e " + reason + " §7| §7Zeit:§e " + roht + " §7Stunden");
					if(Main.isPlayerOnProxy(tc.getName())) {
						if (Main.isPlayerOnline(tc.getName())) {
							tc.setMuteClient(new MuteClient(c, "ChatBan," + t, reason, time));
						}else{
							ChatClient.sendPacket(new PacketPlayerAction(Main.name, tc.getName(), reason, PacketPlayerAction.PlayerAction.MUTE));
						}
					}
				} else if (rtime.equalsIgnoreCase("min")) {
					int roht = Integer.valueOf(t);
					int time = (int) ((roht * 60) + (System.currentTimeMillis() / 1000));
					BanSystem.manualChatBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
					Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gemutet!");
					Main.sendTeamMessage("System", "§7Grund: §e " + reason + " §7| §7Zeit:§e " + roht + " §7Minuten");
					if(Main.isPlayerOnProxy(tc.getName())) {
						if (Main.isPlayerOnline(tc.getName())) {
							tc.setMuteClient(new MuteClient(c, "ChatBan," + t, reason, time));
						}else{
							ChatClient.sendPacket(new PacketPlayerAction(Main.name, tc.getName(), reason, PacketPlayerAction.PlayerAction.MUTE));
						}
					}
				} else {
					p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze bitte §eday§7(Tage) §ehour§7(Stunden) §emin§7(Minuten)"));
				}
			} else {
				p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §eday§7(Tage) §ehour§7(Stunden) §emin§7(Minuten)"));
			}
		}
	}
}
