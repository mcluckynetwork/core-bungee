package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketServerRequest;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 15.09.2015.
 */
public class GoToCommand extends Command {

    public GoToCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (!c.getRang().isTeam()) {
                return;
            }
            if (args.length == 1) {
                String target = args[0];
                if(Main.isPlayerOnProxy(target)) {
                    ProxiedPlayer t = ProxyServer.getInstance().getPlayer(target);
                    if (t != null) {
                        p.connect(t.getServer().getInfo());
                        p.sendMessage(new TextComponent(Main.pre + "§7Du §7wurdest §7zu " + Main.getClient(t).getChatName() + " §7gemovet"));
                    } else {
                        ChatClient.sendPacket(new PacketServerRequest(Main.name, target, "Unknown", PacketServerRequest.ServerAction.GOTO, p.getName()));
                    }
                }else{
                    p.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler §7ist §coffline"));
                }
            } else {
                p.sendMessage(new TextComponent(Main.pre + "§7Nutze §7/goto §7<Spieler>"));
            }
        } else {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}
