package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 02.08.2015.
 */
public class RankCommand extends Command {

    public RankCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if (args.length == 2) {
                String name = args[0];
                String rang = args[1];
                Rang r = Rang.fromName(rang);
                if (r == null) {
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Das ist ein ungültiger Rang"));
                    return;
                }
                UUID uuid = UUIDFetcher.getUUID(name);
                if (!Main.isClientRegistered(uuid)) {
                    Main.getMySQL().Update("INSERT INTO clients (uuid, rang, coins, language, nicked, texturepack, settings) VALUES ('" + uuid + "' , '" + r.getId() + "' , '" + 0 + "' , '" + 0 + "' , '" + 0 + "' , '" + null + "')");
                } else {
                    Client t = Main.getOfflineClient(uuid, name);
                    if(t != null) {
                        t.setRang(r);
                    }else{
                        p.sendMessage(new TextComponent(Main.pre + "§7Client konnte nicht geladen werden"));
                    }
                }
                p.sendMessage(new TextComponent(Main.pre + "§7Der Spieler " + r.getColor() + name + " §7ist nun " + r.getName()));
            } else if (args.length == 1) {
                String name = args[0];
                if(Main.isPlayerOnline(name)) {
                    Client t = Main.getClient(name);
                    p.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler " + t.getChatName() + " §7hat §7den §7Rang " + t.getRang().getName()));
                }else{
                    UUID uuid = UUIDFetcher.getUUID(name);
                    if(uuid == null) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                        return;
                    }
                    Client t = Main.getOfflineClient(uuid, name);
                    if(t != null) {
                        p.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler " + t.getChatName() + " §7hat §7den §7Rang " + t.getRang().getName()));
                    }else{
                        p.sendMessage(new TextComponent(Main.pre + "§7Client konnte nicht geladen werden"));
                    }
                }
            } else {
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/rank <Spieler> <Rang>"));
            }
        }
    }
}
