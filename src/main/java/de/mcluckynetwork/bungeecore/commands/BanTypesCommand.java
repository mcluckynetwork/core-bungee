package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanManager;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanProfile;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Robin on 30.12.2015.
 */
public class BanTypesCommand extends Command {

    public BanTypesCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(!(c.getRang().getId() >= Rang.TESTMODERATOR.getId() && c.getRang() != Rang.DEVELOPER)) {
                return;
            }
            if(args.length == 0) {
                sendTypesList(p);
            }else if(args.length == 2) {
                if(args[0].equalsIgnoreCase("remove")) {
                    if(c.getRang() != Rang.ADMINSTRATOR) {
                        return;
                    }
                    try {
                        int id = Integer.valueOf(args[1]);
                        try {
                            ResultSet rs = Main.getMySQL().Query("SELECT * FROM bantypes WHERE id='" + id + "'");
                            if (rs.next()) {
                                Main.getMySQL().Update("DELETE FROM bantypes WHERE id='" + id + "'");
                                BanType type = BanManager.getBanType(id);
                                if(type != null) {
                                    BanManager.removeBanType(type);
                                }
                            }else{
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Type §e " + id + " §7konnte §7nicht §7gefunden §7werden"));
                            }
                        }catch(SQLException ex) {
                            ex.printStackTrace();
                        }
                    } catch (NumberFormatException ex) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7eine §7gültige §7Zahl §7ein"));
                    }
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
                }
            }else if(args.length == 4) {
                if(args[0].equalsIgnoreCase("add")) {
                    if(c.getRang() != Rang.ADMINSTRATOR) {
                        return;
                    }
                    try{
                        int id = Integer.valueOf(args[1]);
                        String name = args[2];
                        int banprofile = Integer.valueOf(args[3]);
                        try {
                            ResultSet rs = Main.getMySQL().Query("SELECT * FROM bantypes WHERE id='" + id + "' AND banprofile='" + banprofile + "'");
                            if (rs.next()) {
                                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Typ §e" + id + " §7existiert §7bereits"));
                            } else {
                                name = name.replace("_", " ");
                                Main.getMySQL().Update("INSERT INTO bantypes(id, name, banprofile) VALUES('" + id + "' , '" + name + "' , '" + banprofile + "')");
                                p.sendMessage(new TextComponent("§7[§4System§7] §7Der §7Bantyp §e" + id + " §7wurde §7erfolgreich §7hinzugefügt"));
                                BanType type = BanManager.getBanType(id);
                                if(type != null) {
                                    BanManager.removeBanType(type);
                                }
                                BanProfile profile = BanManager.getBanProfile(banprofile);
                                if(profile != null) {
                                    BanManager.addBanType(new BanType(id, name, profile));
                                }else{
                                    p.sendMessage(new TextComponent("§7[§cWarnung§7] §7Der §7BanType §e" + id + " §7konnte §7nicht §7direkt §7übertragen §7werden"));
                                    p.sendMessage(new TextComponent("§7[§cWarnung§7] §7Ein §cNetzwerkneustart §7oder a§7ein §7neu §7hinzufügen §7des §7Ban-Profiles §e " + banprofile + " §7ist §7nötig"));
                                }
                            }
                        }catch(SQLException ex) {
                            ex.printStackTrace();
                        }
                    }catch(NumberFormatException ex) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7gebe §7eine §7gültige §7Zahl §7ein"));
                    }
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Dieses §7Argument §7ist §7nicht §7bekannt"));
            }
        }
    }

    public void sendTypesList(ProxiedPlayer proxiedPlayer) {
        proxiedPlayer.sendMessage(new TextComponent("§7[§4System§7] §7Ban-Typen:"));
        BanManager.getBanTypes().forEach(bantype -> proxiedPlayer.sendMessage(new TextComponent("§7- ID: §e" + bantype.getId() + " §7| §7Name: §e" + bantype.getName() + " §7| §7Ban-Profil: §e" + bantype.getBanProfile().getId())));
    }
}
