package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerAction;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class BanCommand extends Command{

	public BanCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if (c.getRang().getId() >= Rang.MODERATOR.getId() && c.getRang() != Rang.DEVELOPER) {
				if (args.length >= 2) {
					String name = args[0];
					Client tc;
					UUID uuid;
					ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
					if (target != null) {
						uuid = target.getUniqueId();
						tc = Main.getClient(target.getName());
					} else {
						uuid = UUIDFetcher.getUUID(name);
						if (uuid != null) {
							tc = Main.getOfflineClient(uuid, name);
						} else {
							p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
							return;
						}
					}
					if(tc != null) {
						StringBuilder str = new StringBuilder();
						for (int i = 1; i < args.length; i++) {
							str.append(args[i] + " ");
						}
						String reason = str.toString();
						if (c.getRang() == Rang.MODERATOR) {
							if (tc.getRang().getId() >= 3) {
								sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
								return;
							}
						} else if (c.getRang() == Rang.SENIORMODERATOR) {
							if (tc.getRang().getId() >= 4) {
								sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
								return;
							}
						}
						BanSystem.manualBanPlayerBan(p.getUniqueId(), uuid, 0, reason, "Permanent");
						if (Main.isPlayerOnProxy(name)) {
							if (Main.isPlayerOnline(name)) {
								ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(name);
								tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7 Zeit: §4PERMANENT \n§7Entbannungsantrag: http://LuckyWorld.eu"));
							} else {
								ChatClient.sendPacket(new PacketPlayerAction(Main.name, name, reason, PacketPlayerAction.PlayerAction.KICK));
							}
						}
						Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gebannt!");
						Main.sendTeamMessage("System", "§7Zeit: " + "§cPermanent " + "§7| Grund: §e" + reason);
					}else{
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Client §7konnte §7nicht §7geladen §7werden"));
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Bitte §7teile §7diesen §7Bug §7Tyubin §7mit: §7Klasse §7BanCommand"));
					}
				} else {
					p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /ban <Spieler> <Grund>"));
				}
			}
		}
	}

}
