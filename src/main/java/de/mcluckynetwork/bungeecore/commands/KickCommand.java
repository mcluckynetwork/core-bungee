package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerAction;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class KickCommand extends Command{

	public KickCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if(c.getRang().isTeam()) {
				if (args.length == 1) {
					String ps = args[0];
					if (Main.isPlayerOnProxy(ps)) {
						if (Main.isPlayerOnline(ps)) {
							ProxiedPlayer target = ProxyServer.getInstance().getPlayer(ps);
							Client t = Main.getClient(target);
							int id = c.getRang().getId();
							if (id == 4 || id == 5 || id == 6 || id == 8) {
								if (t.getRang().getId() >= 3) {
									sender.sendMessage(new TextComponent(Main.pre + "§7Du §7hast §7nicht §7genügend §7Berechtigungen"));
									return;
								}
							} else if (id == 7) {
								if (t.getRang().getId() >= 4) {
									sender.sendMessage(new TextComponent(Main.pre + "§7Du §7hast §7nicht §7genügend §7Berechtigungen"));
									return;
								}
							}
							target.disconnect(new TextComponent(Main.pre + "\n" + "§7Du §7wurdest §7vom §7Netzwerk §7gekickt"));
							Main.sendTeamMessage("System", c.getChatName() + " §7hat " + t.getChatName() + " §7gekickt!");
						} else {
							Client t = Main.getOfflineClient(UUIDFetcher.getUUID(ps), ps);
							Main.sendTeamMessage("System", c.getChatName() + " §7hat " + t.getChatName() + " §7gekickt!");
							ChatClient.sendPacket(new PacketPlayerAction(Main.name, ps, "NoReason", PacketPlayerAction.PlayerAction.KICK));
						}
					}else{
						sender.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler §7ist §7nicht §7online"));
					}
				} else if (args.length >= 2) {
					String ps = args[0];
					StringBuilder builder = new StringBuilder();
					for (int i = 1; i < args.length; i++) {
						builder.append(args[i] + " ");
					}
					String msg = builder.toString();
					if(Main.isPlayerOnProxy(ps)) {
						if (Main.isPlayerOnline(ps)) {
							Client t = Main.getClient(ps);
							int id = c.getRang().getId();
							if (id == 4 || id == 5 || id == 6 || id == 8) {
								if (t.getRang().getId() >= 3) {
									sender.sendMessage(new TextComponent(Main.pre + "§7Du §7hast §7nicht §7genügend §7Berechtigungen"));
									return;
								}
							} else if (id == 7) {
								if (t.getRang().getId() >= 4) {
									sender.sendMessage(new TextComponent(Main.pre + "§7Du §7hast §7nicht §7genügend §7Berechtigungen"));
									return;
								}
							}
							ProxiedPlayer target = ProxyServer.getInstance().getPlayer(ps);
							target.disconnect(new TextComponent(Main.pre + "\n" + "§7Du §7wurdest §7vom §7Netzwerk §7gekickt \n§7Grund: §e" + msg));
							Main.sendTeamMessage("System", c.getChatName() + " §7hat " + t.getChatName() + " §7gekickt!");
							Main.sendTeamMessage("System", "§7Grund: §e" + msg);
						} else {
							Client t = Main.getOfflineClient(UUIDFetcher.getUUID(ps), ps);
							Main.sendTeamMessage("System", c.getChatName() + " §7hat " + t.getChatName() + " §7gekickt!");
							Main.sendTeamMessage("System", "§7Grund: §e" + msg);
							ChatClient.sendPacket(new PacketPlayerAction(Main.name, ps, msg, PacketPlayerAction.PlayerAction.KICK));
						}
					}else {
						sender.sendMessage(new TextComponent(Main.pre + "§7Der §7Spieler §7ist §7nicht §7online"));
					}
				} else {
					sender.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/kick §7<Spieler> §7<Grund>"));
				}
			}else{
				return;
			}
		}else{
			sender.sendMessage(new TextComponent("Nur für Spieler"));
		}
	}

}
