package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerReport;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.Report;
import de.mcluckynetwork.bungeecore.util.playersystems.ReportSystem;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 16.10.2015.
 */

public class ReportCommand extends Command {

    public ReportCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(args.length <= 6) {
                if(args.length < 2) {
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7/report §7<Name> §7<Grund>"));
                    return;
                }
                if(args[0].equalsIgnoreCase("confirm")) {
                    String name = args[1];
                    Report report = ReportSystem.getReport(p.getName(), name);
                    if(report != null) {
                        c.addReport(report.getReported());
                        ReportSystem.completeReport(report.getReportedid());
                        Client t = Main.getOnOfflineClient(report.getReportedid(), report.getReported());
                        int[] infos = ReportSystem.getReportInfos(report.getReportedid());
                        ChatClient.sendPacket(new PacketPlayerReport(Main.name, t.getChatName(), c.getChatName(), report.getReason(), infos[0], infos[1]));
                        TextComponent component = new TextComponent(Main.rpre);
                        TextComponent click = new TextComponent(t.getChatName());
                        click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/goto " + ChatColor.stripColor(report.getReported())));
                        component.addExtra(click);
                        component.addExtra(new TextComponent(" §7wurde §7von " + c.getChatName() + " §7reportet"));
                        Main.sendReportMessage(component);
                        Main.sendReportMessage(Main.rpre + "§7Grund: §e" + report.getReason() + " §7| §7Total: §e" + infos[0] + " §7| Laufzeit: §e" + infos[1]);
                        ReportSystem.removeReport(report);
                        p.sendMessage(new TextComponent(Main.rpre + "§7Der §7Spieler §7wurde §7erfolgreich §7reportet"));
                    }else{
                        p.sendMessage(new TextComponent(Main.rpre + "§7Du §7hast §7diesen §7Spieler §7nicht §7reportet"));
                    }
                    return;
                }else  if(args[0].equalsIgnoreCase("info")) {
                    if(!c.getRang().isTeam()) {
                        return;
                    }
                    String name = args[1];
                    UUID uuid;
                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
                    if(target != null) {
                        uuid = target.getUniqueId();
                    }else{
                        uuid = UUIDFetcher.getUUID(name);
                        if(uuid != null) {
                            p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                            return;
                        }
                    }
                    Client t = Main.getOnOfflineClient(uuid, target.getName());
                    if(t != null) {
                        int[] rinfos = ReportSystem.getReportInfos(uuid);
                        p.sendMessage(new TextComponent(Main.rpre + t.getChatName() + "§7: §7Total: §e" + rinfos[0] + " §7| §7Laufzeit: §e" + rinfos[1]));
                    }else{
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Spielerinfos §7konnten §7nicht §7geladen §7werden"));
                    }
                }
                String name = args[0];
                if(!c.hasAlreadyReported(name)) {
                    UUID uuid = UUIDFetcher.getUUID(name);
                    if(uuid == null) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                        return;
                    }
                    StringBuilder str = new StringBuilder();
                    for(int i = 1; i < args.length; i ++) {
                        str.append(args[i] + " ");
                    }
                    String reason = str.toString();
                    Client t = Main.getOnOfflineClient(uuid, name);
                    if(t.getRang().getId() >= Rang.YOUTUBER.getId() || p.getName().equalsIgnoreCase(name)) {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Du §7kannst §7diesen §7Spieler §7nicht §7reporten"));
                        return;
                    }
                    ReportSystem.addReport(new Report(p.getName(), uuid, name, reason));
                    p.sendMessage(new TextComponent(Main.rpre + "§7Bist §7du §7sicher §7dass §7du " + t.getChatName() + " §7reporten §7willst? §7Missbrauch §7ist §7strafbar!"));
                    TextComponent component = new TextComponent("§7Bestätige §7mit ");
                    TextComponent accept = new TextComponent("§e/report confirm " + t.getChatName());
                    accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/report confirm " + t.getName()));
                    component.addExtra(accept);
                    p.sendMessage(component);
                }else{
                    p.sendMessage(new TextComponent(Main.rpre + "§7Du §7hast §7diesen §7Spieler §7bereits §7reportet"));
                }
            }else{
                p.sendMessage(new TextComponent(Main.rpre + "§7Nutze §7/report §7<Name> §7<Grund>"));
            }
        }
    }

}
