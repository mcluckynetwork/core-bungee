package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketBlackList;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Robin on 24.07.2015.
 */
public class BlacklistCommand extends Command {

    public BlacklistCommand(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 1) {
                if(args[0].equalsIgnoreCase("list")) {
                    p.sendMessage(new TextComponent(Main.pre + "§7Momentan §7sind §7folgende §7Wörter §7auf §7der §7Blacklist:"));
                    String blacklist = null;
                    for(String word : Main.blacklist) {
                        if(blacklist == null) {
                            blacklist = word + "§7, §7";
                        }else{
                            blacklist = blacklist + "§7, §7" + word;
                        }
                    }
                    p.sendMessage(new TextComponent("§7" + blacklist));
                }else if(args[0].equalsIgnoreCase("reload")) {
                    Main.blacklist.clear();
                    try {
                        ResultSet rs = Main.getMySQL().Query("SELECT * FROM blacklist");
                        while(rs.next()) {
                            String word = rs.getString("word");
                            Main.blacklist.add(word);
                        }
                    }catch(SQLException ex) {
                        ex.printStackTrace();
                    }
                }else{
                    sendHelpMessage(p);
                }
            }else if (args.length == 2) {
                if(args[0].equalsIgnoreCase("add")) {
                    String word = args[1];
                    if (isInTable(word)) {
                        p.sendMessage(new TextComponent(Main.pre + "§7Das Wort §e" + word + " §7ist bereits auf der Blacklist"));
                    } else {
                        Main.getMySQL().Update("INSERT INTO blacklist(word) VALUES ('" + word + "')");
                        Main.blacklist.add(word);
                        ChatClient.sendPacket(new PacketBlackList(Main.name, word, PacketBlackList.BlackListAction.ADD));
                        p.sendMessage(new TextComponent(Main.pre + "§7Das Wort §e" + word + " §7wurde zur Blacklist hinzugefügt"));
                    }
                }else if(args[0].equalsIgnoreCase("remove")) {
                    String word = args[1];
                    if (isInTable(word)) {
                        Main.getMySQL().Update("DELETE FROM blacklist WHERE word='" + word + "'");
                        if (Main.blacklist.contains(word)) {
                            Main.blacklist.remove(word);
                        }
                        ChatClient.sendPacket(new PacketBlackList(Main.name, word, PacketBlackList.BlackListAction.REMOVE));
                        p.sendMessage(new TextComponent(Main.pre + "§7Das Wort §e" + word + " §7wurde von der Blacklist entfernt"));
                    } else {
                        p.sendMessage(new TextComponent(Main.pre + "§7Das Wort §e" + word + " §7ist nicht auf der Blacklist"));
                    }
                }else{
                    sendHelpMessage(p);
                }
            }else{
                sendHelpMessage(p);
            }
        }
    }

    public boolean isInTable(String name) {
        try {
            ResultSet rs = Main.getMySQL().Query("SELECT * FROM blacklist WHERE word='" + name + "'");
            if (rs.next()) {
                return true;
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void sendHelpMessage(ProxiedPlayer p) {
        p.sendMessage(new TextComponent("§6--- §7Blacklist Hilfe §6---"));
        p.sendMessage(new TextComponent("§6/blacklist list §7Listet alle Wörter der Blacklist auf"));
        p.sendMessage(new TextComponent("§6/blacklist add <Wort> §7Fügt ein Wort zur Blacklist hinzu"));
        p.sendMessage(new TextComponent("§6/blacklist remove <Wort> §7Entfernt ein Wort aus der Blacklist"));
        p.sendMessage(new TextComponent("§6--- §7Blacklist Hilfe §6---"));
    }

}
