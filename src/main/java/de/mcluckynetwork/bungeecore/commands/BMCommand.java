package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketBMMessage;
import de.mcluckynetwork.bungeecore.util.BroadcastMessage;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 01.11.2015.
 */
public class BMCommand extends Command {

    public BMCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length == 0) {
                p.sendMessage(new TextComponent("§7[§4INFO§7] §7Aktuelle Broadcast Messages: "));
                for(BroadcastMessage bm : Main.messages) {
                    p.sendMessage(new TextComponent("§7- §7ID: §e" + bm.getId() + " §7| " + bm.getMessage()));
                }
            }else if(args.length >= 2) {
                if(args[0].equalsIgnoreCase("remove")) {
                    int id;
                    try{
                        id = Integer.valueOf(args[1]);
                    }catch(NumberFormatException e) {
                        p.sendMessage(new TextComponent(Main.fpre + "§7Bitte §7gebe §7eine §7Zahl §7an"));
                        return;
                    }
                    BroadcastMessage t = null;
                    for(BroadcastMessage bm : Main.messages) {
                        if(bm.getId() == id) {
                            t = bm;
                        }
                    }
                    if(t == null) {
                        p.sendMessage(new TextComponent("§7[§4INFO§7] §7Die ID §e" + id + " §7konnte nicht gefunden werden"));
                        return;
                    }
                    Main.messages.remove(t);
                    Main.getMySQL().Update("DELETE FROM messages WHERE message='" + t.getMessage() + "'");
                    ChatClient.sendPacket(new PacketBMMessage(Main.name, t.getMessage(), PacketBMMessage.BMAction.REMOVE));
                    p.sendMessage(new TextComponent("§7[§4INFO§7] §7Der §7Broadcast " + t.getMessage() + " §7wurde gelöscht"));
                }else if(args[0].equalsIgnoreCase("add")) {
                    StringBuilder str = new StringBuilder();
                    for(int i = 1; i < args.length; i++) {
                        str.append(args[i] + " ");
                    }
                    String msg = str.toString();
                    msg = msg.replace("&", "§");
                    Main.getMySQL().Update("INSERT INTO messages(message) VALUES ('" + msg + "')");
                    Main.messages.add(new BroadcastMessage(Main.messages.size(), msg));
                    ChatClient.sendPacket(new PacketBMMessage(Main.name, msg, PacketBMMessage.BMAction.ADD));
                    p.sendMessage(new TextComponent("§7[§4INFO§7] §7Der §7Brodcast " + msg + " §7wurde §7hinzugefügt"));
                }else{
                    sendHelpMessage(p);
                }
            }else{
                sendHelpMessage(p);
            }
        }
    }

    private void sendHelpMessage(ProxiedPlayer p) {
        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7BM Hilfe:"));
        p.sendMessage(new TextComponent("§e/bm §7Listet §7"));
        p.sendMessage(new TextComponent("§e/bm remove <ID> §7Löscht §7den §7Brodcast §7mit §7der §7ID"));
        p.sendMessage(new TextComponent("§e/bm add <Message> §7Fügt §7eine §7neue §7Brodcast §7Message §7hinzu"));
    }

}
