package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 03.12.2015.
 */
public class ProxyCommand extends Command {

    public ProxyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang().getId() < Rang.DEVELOPER.getId()) {
                return;
            }
            if(args.length == 1) {
                String name = args[0];
                if(Main.isPlayerOnProxy(name)) {
                    if(Main.isPlayerOnline(name)) {
                        Client t = Main.getClient(name);
                        p.sendMessage(new TextComponent(Main.pre + "§7Der Spieler " + t.getChatName() + " §7ist auf " + Main.name));
                    }else{
                        Client t = Main.getOfflineClient(UUIDFetcher.getUUID(name), name);
                        if(t != null) {
                            p.sendMessage(new TextComponent(Main.pre + "§7Der Spieler " + t.getChatName() + " §7ist auf " + Main.getJedis().get(t.getName())));
                        }else{
                            p.sendMessage(new TextComponent(Main.pre + "§7Client §7konnte §7nicht §7geladen §7werden"));
                        }
                    }
                }else{
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Der §7Spieler §7ist §coffline"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §//proxy §7<Name>"));
            }
        }else{
            System.out.println("Nö kein Bock ^-^");
        }
    }

}
