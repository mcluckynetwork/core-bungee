package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;


public class UnmuteCommand extends Command{

	public UnmuteCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if (!c.getRang().isTeam()) {
				return;
			}
			if (args.length == 1) {
				String name = args[0];
				UUID uuid;
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
				if (target != null) {
					uuid = target.getUniqueId();
				} else {
					uuid = UUIDFetcher.getUUID(name);
					if (uuid == null) {
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
						return;
					}
				}
				Client tc = Main.getOnOfflineClient(uuid, name);
				if(tc != null) {
					if (BanSystem.isInChatBanTable(uuid)) {
						BanSystem.unbanPlayerChatBan(uuid);
						Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7entmutet!");
						if (Main.isPlayerOnline(name)) {
							tc.setMuted(false);
						}
					} else {
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Der Spieler " + tc.getChatName() + " §7ist §7nicht §7gemutet"));
					}
				}else{
					p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Spielerdaten §7konnte §7nicht §7abgerufen §7werden"));
				}
			} else {
				p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze §7bitte §e/unmute §e<Name>"));
			}
		}
	}

}
