package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketActionBarUpdate;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 31.10.2015.
 */

public class ABCommand extends Command {

    public ABCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if(c.getRang() != Rang.ADMINSTRATOR) {
                return;
            }
            if(args.length >= 1) {
                String str = null;
                for(int i = 0; i < args.length; i++) {
                    if(str == null) {
                        str = args[i];
                    }else{
                        str = str + " " + args[i];
                    }
                }
                if(str != null) {
                    str = str.replace("&", "§");
                    Main.getMySQL().Update("UPDATE manager SET second='" + str + "' WHERE first='" + "ab" + "'");
                    ChatClient.sendPacket(new PacketActionBarUpdate(str));
                    p.sendMessage(new TextComponent(Main.pre + "§7Neue Actionbar: " + str));
                }else{
                    p.sendMessage(new TextComponent(Main.pre + "§7Message §7konnte §7nicht §7verarbeitet §7werden"));
                }
            }else{
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /ab §7<Nachricht>"));
            }
        }
    }
}
