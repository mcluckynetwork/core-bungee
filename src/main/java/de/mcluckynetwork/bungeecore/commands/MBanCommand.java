package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerAction;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Robin on 30.12.2015.
 */
public class MBanCommand extends Command {

    public MBanCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            Client c = Main.getClient(p);
            if (!(c.getRang().getId() >= Rang.MODERATOR.getId() && c.getRang() != Rang.DEVELOPER)) {
                return;
            }
            if (args.length >= 4) {
                String name = args[0];
                Client tc = null;
                UUID uuid;
                ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);
                if (target != null) {
                    uuid = target.getUniqueId();
                    if (uuid != null) {
                        tc = Main.getClient(target.getName());
                    } else {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                    }
                } else {
                    uuid = UUIDFetcher.getUUID(name);
                    if (uuid != null) {
                        tc = Main.getOfflineClient(uuid, name);
                    } else {
                        p.sendMessage(new TextComponent("§7[§aHilfe§7] §7UUID §7konnte §7nicht §7gefunden §7werden"));
                    }
                }
                String t = args[1];
                String rtime = args[2];
                StringBuilder str = new StringBuilder();
                for (int i = 3; i < args.length; i++) {
                    str.append(args[i] + " ");
                }
                String reason = str.toString();
                if (c.getRang() == Rang.MODERATOR) {
                    if (tc.getRang().getId() >= 3) {
                        sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
                        return;
                    }
                } else if (c.getRang() == Rang.SENIORMODERATOR) {
                    if (tc.getRang().getId() >= 4) {
                        sender.sendMessage(new TextComponent("§7[§aHilfe§7] §cDu §chast §cnicht §cgenügend §cBerechtigungen"));
                        return;
                    }
                }
                if (rtime.equalsIgnoreCase("month")) {
                    int roht = Integer.valueOf(t);
                    int time = (int) ((roht * 2592000) + (System.currentTimeMillis() / 1000));
                    BanSystem.manualBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
                    if(Main.isPlayerOnProxy(name)) {
                        if (Main.isPlayerOnline(name)) {
                            int timez = roht * 2592000;
                            int[] zeit = BanSystem.secondsToArrays(timez);
                            ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(name);
                            tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7Verbleibende Zeit: §e" + zeit[0] + " §7Monate §e" + zeit[1] + " §7Tage §e" + zeit[2] + " §7Stunden §e" + zeit[3] + " §7Minuten §e" + zeit[4] + " §7Sekunden \n§7Entbannungsantrag: http://LuckyWorld.eu"));
                        }else{
                            ChatClient.sendPacket(new PacketPlayerAction(Main.name, name, reason, PacketPlayerAction.PlayerAction.BAN));
                        }
                    }
                    Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gebannt!");
                    Main.sendTeamMessage("System", "§7Zeit: §e" + roht + " §eMonate " + "§7| Grund: §e" + reason);
                } else if (rtime.equalsIgnoreCase("day")) {
                    int roht = Integer.valueOf(t);
                    int time = (int) ((roht * 86400) + (System.currentTimeMillis() / 1000));
                    BanSystem.manualBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
                    if(Main.isPlayerOnProxy(name)) {
                        if (Main.isPlayerOnline(name)) {
                            int timez = roht * 86400;
                            int[] zeit = BanSystem.secondsToArrays(timez);
                            ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(name);
                            tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7Verbleibende Zeit: §e" + zeit[0] + " §7Monate §e" + zeit[1] + " §7Tage §e" + zeit[2] + " §7Stunden §e" + zeit[3] + " §7Minuten §e" + zeit[4] + " §7Sekunden \n§7Entbannungsantrag: http://LuckyWorld.eu"));
                        }else{
                            ChatClient.sendPacket(new PacketPlayerAction(Main.name, name, reason, PacketPlayerAction.PlayerAction.BAN));
                        }
                    }
                    Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gebannt!");
                    Main.sendTeamMessage("System", "§7Zeit: §e" + roht + " §eTage " + "§7| Grund: §e" + reason);
                } else if (rtime.equalsIgnoreCase("hour")) {
                    int roht = Integer.valueOf(t);
                    int time = (int) ((roht * 3600) + (System.currentTimeMillis() / 1000));
                    BanSystem.manualBanPlayerBan(p.getUniqueId(), uuid, time, reason, t + rtime);
                    if(Main.isPlayerOnProxy(name)) {
                        if (Main.isPlayerOnline(name)) {int timez = roht * 3600;
                            int[] zeit = BanSystem.secondsToArrays(timez);
                            ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(name);
                            tp.disconnect(new TextComponent(Main.pre + "\n" + "§cDu bist vom Netzwerk gebannt \n §7Grund: " + reason + "\n \n §7Verbleibende Zeit: §e" + zeit[0] + " §7Monate §e" + zeit[1] + " §7Tage §e" + zeit[2] + " §7Stunden §e" + zeit[3] + " §7Minuten §e" + zeit[4] + " §7Sekunden \n§7Entbannungsantrag: http://LuckyWorld.eu"));
                        }else{
                            ChatClient.sendPacket(new PacketPlayerAction(Main.name, name, reason, PacketPlayerAction.PlayerAction.BAN));
                        }
                    }
                    Main.sendTeamMessage("System", c.getChatName() + " §7hat " + tc.getChatName() + " §7gebannt!");
                    Main.sendTeamMessage("System", "§7Zeit: §e" + roht + " §eStunden " + "§7| Grund: §e" + reason);
                } else {
                    p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Zeit-Angaben: §7month§e(Monate) §7day§e(Tage) §7hour§e(Stunden) §7perm§e(Permanent)"));
                }
            } else {
                p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Nutze /mban <Spieler> <Zeit> <Grund>"));
            }
        }
    }

}
