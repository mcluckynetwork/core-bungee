package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HelpCommand extends Command{

	public HelpCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("rang") || args[0].equalsIgnoreCase("rank")) {
					if (c.getRang().getId() >= Rang.ARCHITEKT.getId()) {
						p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Übersicht §7der §7erweiteren §7Commands:"));
						p.sendMessage(new TextComponent("§e/server §7Server §7wechseln"));
						p.sendMessage(new TextComponent("§e/t §7Im §7Teamchat §7schreiben"));
						p.sendMessage(new TextComponent("§e/notify §7Stellt §7den §7Teamchat §7ab"));
					}
					if (c.getRang().getId() >= Rang.TESTMODERATOR.getId()) {
						p.sendMessage(new TextComponent("§e/tban §7Bannt §7einen §7Spieler §7temporär"));
						p.sendMessage(new TextComponent("§e/unban §7Entbannt einen Spieler"));p.sendMessage(new TextComponent("§e/mute §7Mutet einen Spieler"));
						p.sendMessage(new TextComponent("§e/unmute §7Entmutet einen Spieler"));
						p.sendMessage(new TextComponent("§e/kick §7Kickt einen Spieler"));
						p.sendMessage(new TextComponent("§e/goto §7Movet §7 §7auf §7den §7Server §7des §7Spielers"));
						p.sendMessage(new TextComponent("§e/whereis §7Gibt §7dir §7den §7Server §7des §7Spielers §7aus"));
						p.sendMessage(new TextComponent("§e/vanish §7Du §7wirst §7Spielern §7in §7der §7Lobby §7nicht §7angezeigt"));
					}
					if (c.getRang().getId() >= Rang.SENIORMODERATOR.getId()) {
						p.sendMessage(new TextComponent("§e/ban §7Bannt §7einen §7Spieler"));
					}
					if (c.getRang().getId() >= Rang.ADMINSTRATOR.getId()) {
						p.sendMessage(new TextComponent("§e/whitelist §7Spieler zur Whitelist hinzufügen"));
						p.sendMessage(new TextComponent("§e/code §7Code für die Whitelist hinzufügen"));
					}
					if (c.getRang().getId() < Rang.ARCHITEKT.getId()) {
						sendHelpMessage(p);
					}
				}
			} else {
				sendHelpMessage(p);
			}
		} else {
			sender.sendMessage(new TextComponent("Nur für Spieler"));
		}
	}
	
	public void sendHelpMessage(ProxiedPlayer p) {
		p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Allgemeine Hilfe:"));
		p.sendMessage(new TextComponent("§e/friend §7Command §7um §7alle §7Freunde §7zu §7managen"));
		p.sendMessage(new TextComponent("§e/msg §7Schickt §7einem §7Freund §7eine §7private §7Nachricht"));
		p.sendMessage(new TextComponent("§e/party §7Zeige die §7Übersicht §7der §7Party-Funktionen"));
		p.sendMessage(new TextComponent("§e/ping §7Sieh §7deine §7aktuelle §7Verbindung §7zum §7Server §7ein"));
		p.sendMessage(new TextComponent("§e/report §7Reportet §7einen §7Spieler"));
		Client c = Main.getClient(p);
		if(c.getRang().isTeam()) {
			p.sendMessage(new TextComponent("§e/help rang §7Zeigt §7dir §7die §7erweiterte §7Hilfe"));
		}
	}

}
