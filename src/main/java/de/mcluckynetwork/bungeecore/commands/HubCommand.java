package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.LobbyManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Robin on 15.09.2015.
 */
public class HubCommand extends Command {

    private String name;

    public HubCommand(String name) {
        super(name);
        this.name = name;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            if (args.length == 0) {
                if (p.getServer().getInfo().getName().contains("LOBBY")) {
                    p.sendMessage(new TextComponent(Main.pre + "§7Du §7bist §7bereits §7in §7einer §7Lobby"));
                    return;
                }
                p.connect(ProxyServer.getInstance().getServerInfo(LobbyManager.getRandomLobby().getName()));
            } else {
                p.sendMessage(new TextComponent(Main.pre + "§7Nutze /" + this.name));
            }
        } else {
            sender.sendMessage(new TextComponent("Nur für Spieler"));
        }
    }

}
