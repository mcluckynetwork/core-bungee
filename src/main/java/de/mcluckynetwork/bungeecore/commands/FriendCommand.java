package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.clients.Setting;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketFriendActionPerform;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FriendCommand extends Command {

	public FriendCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(p);
			if(args.length == 1){
				if(args[0].equalsIgnoreCase("list")) {
					showFriendList(p, 1);
				}else{
					sendHelpMessage(p);
				}
			}else if(args.length == 2) {
				if(args[0].equalsIgnoreCase("list")) {
					try{
						showFriendList(p, Integer.valueOf(args[1]));
					}catch(NumberFormatException e) {
						p.sendMessage(new TextComponent(Main.fpre + "§7Bitte §7gebe §7eine §7Zahl §7an"));
					}
				}else if(args[0].equalsIgnoreCase("add")) {
					if(args[1].equalsIgnoreCase(p.getName())) {
						p.sendMessage(new TextComponent(Main.fpre + "§7Du §7kannst §7dir §7selber §7keine §7Anfrage §7senden"));
						return;
					}
					UUID uuid;
					if(Main.isPlayerOnline(args[1])) {
						uuid = ProxyServer.getInstance().getPlayer(args[1]).getUniqueId();
					}else{
						uuid = UUIDFetcher.getUUID(args[1]);
						if(uuid == null) {
							p.sendMessage(new TextComponent("§7[§aHilfe§7] §cUUID §ckonnte §cnicht §cabgerufen §cwerden"));
							return;
						}
					}
					if(!FriendSystem.areFriends(p.getName(), args[1])) {
						if(!FriendSystem.askedPlayer(p.getName(), args[1])) {
							if (Main.isPlayerOnProxy(args[1])) {
								if (Main.isPlayerOnline(args[1])) {
									ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
									Client t = Main.getClient(args[1]);
										if (t.isActiveSetting(Setting.FRIENDREQUESTS) || c.getRang().getId() >= Rang.YOUTUBER.getId()) {FriendSystem.addFriend(p.getName(), args[1]);
											p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7dem §7Spieler " + t.getChatName() + " §7eine §7Anfrage §7gesendet"));
											target.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7eine §7Anfrage §7von " + c.getChatName() + " §7erhalten"));
											TextComponent accept = new TextComponent(Main.fpre + "§7Annehmen: §e/friend §eaccept " + c.getChatName());
											TextComponent deny = new TextComponent(Main.fpre + "§7Ablehnen: §e/friend §edeny " + c.getChatName());
											accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + p.getName()));
											deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + p.getName()));
											target.sendMessage(accept);
											target.sendMessage(deny);
										} else {
											p.sendMessage(new TextComponent(Main.fpre + "§7Der §7Spieler " + t.getChatName() + " §7nimmt §7keine §7Anfragen §7an"));
										}
								} else {
									Client t = Main.getOfflineClient(UUIDFetcher.getUUID(args[1]), args[1]);
									if(t != null) {
										FriendSystem.addFriend(p.getName(), args[1]);
										p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7dem §7Spieler " + t.getChatName() + " §7eine §7Anfrage §7gesendet"));
										ChatClient.sendPacket(new PacketFriendActionPerform(Main.name, c.getChatName(), args[1], PacketFriendActionPerform.FriendAction.ADD));
									}else{
										p.sendMessage(new TextComponent(Main.fpre + "§7Spielerdaten §7konnten §7nicht §7geladen §7werden"));
									}
								}
							}else{
								Client t = Main.getOfflineClient(uuid, args[1]);
								if(t != null) {
									if (t.isActiveSetting(Setting.FRIENDREQUESTS) || c.getRang().getId() >= Rang.YOUTUBER.getId()) {
										FriendSystem.addFriend(p.getName(), args[1]);
										p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7dem §7Spieler " + t.getChatName() + " §7eine §7Anfrage §7gesendet"));
									} else {
										p.sendMessage(new TextComponent(Main.fpre + "§7Der §7Spieler " + t.getChatName() + " §7nimmt §7keine §7Anfragen §7an"));
									}
								}else{
									p.sendMessage(new TextComponent(Main.fpre + "§7Spielerdaten §7konnten §7nicht §7geladen §7werden"));
								}
							}
						}else{
							Client o = Main.getOfflineClient(uuid, args[1]);
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + o.getChatName() + " §7bereits §7angefragt"));
						}
					}else{
						Client o = Main.getOfflineClient(uuid, args[1]);
						p.sendMessage(new TextComponent(Main.fpre + "§7Du §7bist §7bereits §7mit " + o.getChatName() + " §7befreundet"));
					}
				}else if(args[0].equalsIgnoreCase("accept")) {
					if(args[1].equalsIgnoreCase("all")) {
						List<FriendSystem.Friend> requests = FriendSystem.getAnfragen(p.getName());
						if(requests.size() == 0) {
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7momentan §7keine §7Anfragen"));
							return;
						}
						for(FriendSystem.Friend a : requests) {
							if(Main.isPlayerOnline(a.getName())) {
								FriendSystem.acceptFriendOnline(p.getName(), a.getName());
							}else{
								FriendSystem.acceptFriendOffline(p.getName(), a.getName());
							}
						}
						p.sendMessage(new TextComponent(Main.fpre + "§7Alle §7Anfragen §7wurden §7angenommen"));
						return;
					}
					UUID uuid;
					ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(args[1]);
					if(tp != null) {
						uuid = tp.getUniqueId();
					}else{
						uuid = UUIDFetcher.getUUID(args[1]);
						if(uuid == null) {
							p.sendMessage(new TextComponent("§7[§aHilfe§7] §cUUID §ckonnte §cnicht §cabgerufen §cwerden"));
							return;
						}
					}
					if(!FriendSystem.areFriends(p.getName(), args[1])) {
						if(FriendSystem.askedPlayerOption2(p.getName(), args[1])) {
							if(Main.isPlayerOnProxy(args[1])) {
								ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
								if(target != null) {
									FriendSystem.acceptFriendOnline(p.getName(), args[1]);
									Client t = Main.getClient(args[1]);
									target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7hat §7deine §7Anfrage §7angenommen"));
									p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + t.getChatName() + " §7angenommen"));
								} else {
									FriendSystem.acceptFriendOnline(p.getName(), args[1]);
									Client t = Main.getOfflineClient(uuid, args[1]);
									p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + t.getChatName() + " §7angenommen"));
									ChatClient.sendPacket(new PacketFriendActionPerform(Main.name, c.getChatName(), args[1], PacketFriendActionPerform.FriendAction.ACCEPT));
								}
							}else{
								FriendSystem.acceptFriendOffline(p.getName(), args[1]);
								Client o = Main.getOfflineClient(uuid, args[1]);
								p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + o.getChatName() + " §7angenommen"));
							}
						}else{
							Client o = Main.getOfflineClient(uuid, args[1]);
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + o.getChatName() + " §7nicht §7angefragt"));
						}
					}else{
						Client o = Main.getOfflineClient(uuid, args[1]);
						p.sendMessage(new TextComponent(Main.fpre + "§7Du §7bist §7bereits §7mit " + o.getChatName() + " §7befreundet"));
					}
				}else if(args[0].equalsIgnoreCase("deny")) {
					if(args[1].equalsIgnoreCase("all")) {
						List<FriendSystem.Friend> requests = FriendSystem.getAnfragen(p.getName());
						if(requests.size() == 0) {
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7momentan §7keine §7Anfragen"));
							return;
						}
						for(FriendSystem.Friend a : requests) {
							if(Main.isPlayerOnline(a.getName())) {
								FriendSystem.denyFriendOnline(p.getName(), a.getName());
							}else{
								FriendSystem.denyFriendOffline(p.getName(), a.getName());
							}
						}
						p.sendMessage(new TextComponent(Main.fpre + "§7Alle §7Anfragen §7wurden §7abgelehnt"));
						return;
					}
					UUID uuid;
					if(Main.isPlayerOnline(args[1])) {
						uuid = ProxyServer.getInstance().getPlayer(args[1]).getUniqueId();
					}else{
						uuid = UUIDFetcher.getUUID(args[1]);
						if(uuid == null) {
							p.sendMessage(new TextComponent("§7[§aHilfe§7] §cUUID §ckonnte §cnicht §cabgerufen §cwerden"));
							return;
						}
					}
					if(!FriendSystem.areFriends(p.getName(), args[1])) {
						if(FriendSystem.askedPlayer(p.getName(), args[1])) {
							if(Main.isPlayerOnProxy(args[1])) {
								ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);
								if (target != null) {
									FriendSystem.denyFriendOnline(p.getName(), args[1]);
									Client t = Main.getClient(args[1]);
									target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7hat §7deine §7Anfrage §7abgelehnt"));
									p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + t.getChatName() + " §7abgelehnt"));
								} else {
									FriendSystem.denyFriendOnline(p.getName(), args[1]);
									Client t = Main.getOfflineClient(uuid, args[1]);
									p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + t.getChatName() + " §7abgelehnt"));
									ChatClient.sendPacket(new PacketFriendActionPerform(Main.name, c.getChatName(), args[1], PacketFriendActionPerform.FriendAction.DENY));
								}
							}else{
								FriendSystem.denyFriendOffline(p.getName(), args[1]);
								Client o = Main.getOfflineClient(uuid, args[1]);
								p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7die §7Anfrage §7von " + o.getChatName() + " §7abgelehnt"));
							}
						}else{
							Client o = Main.getOfflineClient(uuid, args[1]);
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + o.getChatName() + " §7nicht §7angefragt"));
						}
					}else{
						Client o = Main.getOfflineClient(uuid, args[1]);
						p.sendMessage(new TextComponent(Main.fpre + "§7Du §7bist §7bereits §7mit " + o.getChatName() + " §7befreundet"));
					}
				}else if(args[0].equalsIgnoreCase("remove")) {
					UUID uuid;
					if(Main.isPlayerOnline(args[1])) {
						uuid = ProxyServer.getInstance().getPlayer(args[1]).getUniqueId();
					}else{
						uuid = UUIDFetcher.getUUID(args[1]);
						if(uuid == null) {
							p.sendMessage(new TextComponent("§7[§aHilfe§7] §cUUID §ckonnte §cnicht §cabgerufen §cwerden"));
							return;
						}
					}
					if(FriendSystem.areFriends(p.getName(), args[1])) {
						if(Main.isPlayerOnline(args[1])) {
							ProxiedPlayer target = ProxyServer.getInstance().getPlayer(uuid);
							if (target != null) {
								FriendSystem.removeFriendOnline(p.getName(), args[1]);
								Client t = Main.getClient(args[1]);
								target.sendMessage(new TextComponent(Main.fpre + "§7Der §7Spieler " + c.getChatName() + " §7hat §7dich §7von §7seinen §7Freunden §7entfernt"));
								p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + t.getChatName() + " §7von §7deinen §7Freunden §7entfernt"));
							} else {
								FriendSystem.removeFriendOnline(p.getName(), args[1]);
								Client t = Main.getOfflineClient(uuid, args[1]);
								p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + t.getChatName() + " §7von §7deinen §7Freunden §7entfernt"));
								ChatClient.sendPacket(new PacketFriendActionPerform(Main.name, c.getChatName(), args[1], PacketFriendActionPerform.FriendAction.REMOVE));
							}
						}else{
							FriendSystem.removeFriendOffline(p.getName(), args[1]);
							Client o = Main.getOfflineClient(uuid, args[1]);
							p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7den §7Spieler " + o.getChatName() + " §7von §7deinen §7Freunden §7entfernt"));
						}
					}else{
						Client o = Main.getOfflineClient(uuid, args[1]);
						p.sendMessage(new TextComponent(Main.fpre + "§7Du §7bist §7nicht §7mit " + o.getChatName() + " §7befreundet"));
					}
				}else{
					sendHelpMessage(p);
				}
			}else{
				sendHelpMessage(p);
			}
		}else{
			sender.sendMessage(new TextComponent("Nur für Spieler"));
		}
	}


	private void sendHelpMessage(ProxiedPlayer p) {
		p.sendMessage(new TextComponent("§7[§aHilfe§7] §7Freundesystem Hilfe:"));
		p.sendMessage(new TextComponent("§e/friend add <Name> §7Fragt §7einen §7Spieler §7als §7Freund §7an"));
		p.sendMessage(new TextComponent("§e/friend accept <Name> §7Nimmt §7die §7Anfrage §7des §7Spielers §7an"));
		p.sendMessage(new TextComponent("§e/friend deny <Name> §7Lehnt §7die §7Anfrage §7des §7Spielers §7ab"));
		p.sendMessage(new TextComponent("§e/friend remove <Name> §7Löscht §7einen §7Spieler §7aus §7deinen §7Freunden"));
		p.sendMessage(new TextComponent("§e/friend list §7Listet §7deine §7Freunde §7auf"));
	}

	private void showFriendList(ProxiedPlayer p, int page) {
		int start = (page * 8) - 1;
		int s = (start - 8) + 2;
		List<FriendSystem.Friend> friends = FriendSystem.getFriends(p.getName());
		if(friends.size() == 0) {
			p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7noch §7keine §7Freunde"));
			return;
		}
		if(friends.size() < s) {
			p.sendMessage(new TextComponent(Main.fpre + "§7Du §7hast §7nicht §7so §7viele §7Freunde"));
			return;
		}
		List<Client> clients = new ArrayList<>();
		for(FriendSystem.Friend n : friends) {
			Client t = Main.getOnOfflineClient(n.getUuid(), n.getName());
			if(t != null) {
				clients.add(t);
			}
		}
		p.sendMessage(new TextComponent(Main.fpre + "§7Liste deiner Freunde:"));
		if(friends.size() != 0) {
			List<Client> fs = returnFromArrayList(clients, s, start);
			for(Client c : fs) {
				if(c.getOnline() == 1) {
					p.sendMessage(new TextComponent("§7- " + c.getChatName() + " " + getOnline(c.getName())));
				}else{
					p.sendMessage(new TextComponent("§7- " + c.getChatName()));
				}
			}
			if(friends.size() > start) {
				p.sendMessage(new TextComponent(Main.fpre + "§7Weitere §7Freunde: §e/friend §elist §e" + (page + 1)));
			}
		}
	}

	public static List<Client> returnFromArrayList(List<Client> list, int start, int end) {
		List<Client> returnto = new ArrayList<>();
		for(int i = 0; i < list.size(); i++) {
			if(i >= start && i <= end) {
				returnto.add(list.get(i));
			}
		}
		return returnto;
	}

	public String getOnline(String name) {
		if(Main.isPlayerOnProxy(name)) {
			return " §7(ONLINE)";
		}
		return " §7(Offline)";
	}
}
