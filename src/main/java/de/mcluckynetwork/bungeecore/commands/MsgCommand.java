package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketFriendMsg;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class MsgCommand extends Command{

	public MsgCommand(String name) { super(name); }

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			if (args.length >= 2) {
				String ps = args[0];
				UUID uuid = null;
				if (!Main.isPlayerOnProxy(ps)) {
					uuid = UUIDFetcher.getUUID(ps);
					if (uuid == null) {
						p.sendMessage(new TextComponent(Main.fpre + "§7UUID §7konnte §7nicht §7gefunden §7werden"));
					} else {
						Client c = Main.getOfflineClient(uuid, ps);
						p.sendMessage(new TextComponent(Main.fpre + "§7Der §7Spieler " + c.getChatName() + " §7ist §coffline"));
					}
					return;
				}
				if (!FriendSystem.areFriends(p.getName(), ps)) {
					Client c = Main.getOfflineClient(uuid, ps);
					p.sendMessage(new TextComponent(Main.fpre + "§7Du §7bist §7nicht §7mit " + c.getChatName() + " §7befreundet"));
					return;
				}
				Client c = Main.getClient(p);
				StringBuilder builder = new StringBuilder();
				for (int i = 1; i < args.length; i++) {
					builder.append(args[i] + " §7");
				}
				String msg = builder.toString();
				ProxiedPlayer target = ProxyServer.getInstance().getPlayer(ps);
				Client t;
				if(target != null) {
					t = Main.getClient(target);
					t.setRet(p.getName());
					target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
					p.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
				}else{
					t = Main.getOfflineClient(UUIDFetcher.getUUID(ps), ps);
					if(t != null) {
						p.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7-> " + t.getChatName() + "§8: §7" + msg));
						ChatClient.sendPacket(new PacketFriendMsg(Main.name, c.getChatName(), ps, msg));
					}
				}
			}
		}else{
			sender.sendMessage(new TextComponent("Momentan nur für Spieler"));
		}
	}

}
