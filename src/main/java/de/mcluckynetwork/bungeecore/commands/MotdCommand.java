package de.mcluckynetwork.bungeecore.commands;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketMotdUpdate;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MotdCommand extends Command{

	public MotdCommand(String name) {
		super(name);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			Client c = Main.getClient(sender.getName());
			if (c.getRang().getId() < Rang.SENIORMODERATOR.getId()) {
				return;
			}
			if (args.length > 0) {
				StringBuilder str = new StringBuilder();
				for (int i = 0; i < args.length; i++) {
					str.append(args[i] + " ");
				}
				String message = str.toString();
				Main.motd = message.replace("&", "§");
				Main.getMySQL().Update("UPDATE manager SET second='" + Main.motd + "' WHERE first='motd'");
				Main.sendTeamMessage("System", "§7Neue §7Motd: " + Main.motd);
				ChatClient.sendPacket(new PacketMotdUpdate(Main.name, Main.motd));
			} else if (args.length == 0) {
				p.sendMessage(new TextComponent(Main.pre + "§7Jetztige Motd: " + Main.motd));
			}
		}
	}

}
