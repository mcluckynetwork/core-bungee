package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketFriendAction;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketPlayerReportJoin;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import de.mcluckynetwork.bungeecore.util.playersystems.ReportSystem;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Created by Robin on 24.07.2015.
 */
public class PostLoginListener implements Listener {

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Jedis jedis = Main.getJedis();
        if(jedis != null) {
            jedis.set(p.getName(), Main.name);
        }
        Main.online = Main.online + 1;
        Client c = Main.getClient(p);
        sendOnlineMessage(p, c);
        sendAcceptedMessage(p, c);
        sendDenyedMessage(p, c);
        sendReceivedMessage(p, c);
        sendRemovedMessage(p, c);
        int totalreports = ReportSystem.getTotalReports(p.getUniqueId());
        if (totalreports >= 2) {
            Packet reportpacket = new PacketPlayerReportJoin(Main.name, c.getChatName(), totalreports);
            ChatClient.sendPacket(reportpacket);
            TextComponent component = new TextComponent(Main.rpre);
            TextComponent click = new TextComponent(c.getChatName());
            click.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/goto " + ChatColor.stripColor(c.getChatName())));
            component.addExtra(click);
            component.addExtra(new TextComponent(" §7ist §7nun §7wieder §7online"));
            Main.sendReportMessage(component);
            Main.sendReportMessage(Main.rpre + "§7Er §7wurde §e" + totalreports + " §7mal §7reportet");
        }
        Packet friendonlinepacket = new PacketFriendAction(Main.name, p.getName(), PacketFriendAction.Action.JOIN, ProxyServer.getInstance().getOnlineCount());
        ChatClient.sendPacket(friendonlinepacket);
    }

    public static void sendOnlineMessage(ProxiedPlayer p, Client c) {
        List<FriendSystem.Friend> friends = FriendSystem.getFriends(p.getName());
        friends.forEach(all -> {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(all.getName());
            if(target != null) {
                target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7ist §7nun §aonline"));
            }
        });
    }

    public static void sendAcceptedMessage(ProxiedPlayer p, Client c) {
        List<FriendSystem.Friend> accepted = FriendSystem.getAccepted(p.getName());
        if(accepted.size() != 0) {
            StringBuilder str = new StringBuilder();
            for(int i = 0; i < accepted.size(); i++) {
                FriendSystem.Friend friend = accepted.get(i);
                if(i == accepted.size()) {
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName());
                }else{
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName() + "§7, ");
                }
            }
            p.sendMessage(new TextComponent("§7Die §7folgenden §7Spieler §7haben §7deine §7Anfrage §7angenommen: " + str.toString()));
        }
    }

    public static void sendDenyedMessage(ProxiedPlayer p, Client c) {
        List<FriendSystem.Friend> denyed = FriendSystem.getDenyed(p.getName());
        if(denyed.size() != 0) {
            StringBuilder str = new StringBuilder();
            for(int i = 0; i < denyed.size(); i++) {
                FriendSystem.Friend friend = denyed.get(i);
                if(i == denyed.size()) {
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName());
                }else{
                    str.append(Main.getOfflineClient(friend.getUuid(), friend.getName()).getChatName() + "§7, ");
                }
            }
            p.sendMessage(new TextComponent("§7Die §7folgenden §7Spieler §7haben §7deine §7Anfrage §7abgelehnt: " + str.toString()));
        }
    }

    public static void sendReceivedMessage(ProxiedPlayer p, Client c) {
        List<FriendSystem.Friend> requests = FriendSystem.getAnfragen(p.getName());
        if(requests.size() != 0) {
            StringBuilder str = new StringBuilder();
            for(int i = 0; i < requests.size(); i++) {
                FriendSystem.Friend friend = requests.get(i);
                if(i == requests.size()) {
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName());
                }else{
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName() + "§7, ");
                }
            }
            p.sendMessage(new TextComponent("§7Die §7folgenden §7Spieler §7haben §7dir §7eine §7Anfrage §7gesendet: " + str.toString()));
        }
    }

    public static void sendRemovedMessage(ProxiedPlayer p, Client c) {
        List<FriendSystem.Friend> removed = FriendSystem.getRemoved(p.getName());
        if(removed.size() != 0) {
            StringBuilder str = new StringBuilder();
            for(int i = 0; i < removed.size(); i++) {
                FriendSystem.Friend friend = removed.get(i);
                if(i == removed.size()) {
                    str.append(Main.getOnOfflineClient(friend.getUuid(), friend.getName()).getChatName());
                }else{
                    str.append(Main.getOfflineClient(friend.getUuid(), friend.getName()).getChatName() + "§7, ");
                }
            }
            p.sendMessage(new TextComponent("§7Die §7folgenden §7Spieler §7haben §7die §7Freundschaft §7beendet: " + str.toString()));
        }
    }

}
