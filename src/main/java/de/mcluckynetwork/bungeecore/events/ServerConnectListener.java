package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.Lobby;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.LobbyManager;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem.GameServer;
import de.mcluckynetwork.bungeecore.util.serversystems.maxserversystem.GameServerManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 14.09.2015.
 */
public class ServerConnectListener implements Listener {

    @EventHandler
    public void onServerConnect(ServerConnectEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Client c = Main.getClient(p);
        if(e.getTarget().getName().contains("SilentLobby")) {
            if(!(c.getRang().isTeam() || c.getRang() == Rang.YOUTUBER)) {
                e.setCancelled(true);
                p.sendMessage(new TextComponent("§c>> §7Diese §7Lobby §7ist §7nur §7für §7Teammitglieder §7und §7YouTuber §7zugänglich"));
            }
        }
        GameServer gs = GameServerManager.getServer(e.getTarget().getName());
        if(c.isJoining()) {
            Lobby l = LobbyManager.getRandomLobby();
            if (l != null && l.getName() != null) {
                if (!e.getTarget().getName().equalsIgnoreCase(l.getName())) {
                    e.setTarget(ProxyServer.getInstance().getServerInfo(l.getName()));
                }
            }
            c.setJoining(false);
        }
        if(!e.getTarget().getName().contains("Lobby")) {
            if(gs != null) {
                if (gs.getState() == State.FULL) {
                    if (c.getRang().getId() >= 1) {
                        List<ProxiedPlayer> ps = new ArrayList<>();
                        e.getTarget().getPlayers().stream().forEach(pc -> ps.add(pc));
                        ProxiedPlayer kick = getNonPremiumPlayer(ps, c);
                        if (kick == null) {
                            e.setCancelled(true);
                            p.sendMessage(new TextComponent("§c>> §7Auf §7diesem §7Server §7konnte §7kein §7Platz §7für §7dich §7gemacht §7werden"));
                        } else {
                            kick.connect(LobbyManager.getRandomLobby().getServer());
                            Client k = Main.getClient(kick);
                            kick.sendMessage(new TextComponent("§>> §7Du §7wurdest §7gekickt, §7da §7ein §7Spieler §7höheren §7Ranges §7den §7Server §7betreten §7hat!"));
                            if (k.getRang() == Rang.SPIELER) {
                                kick.sendMessage(new TextComponent("§c>> §7Kaufe dir §6Premium §7um §7nicht §7mehr §7geckickt §7zu §7werden"));
                            }
                        }
                    } else {
                        e.setCancelled(true);
                        p.sendMessage(new TextComponent("&c>> &7Dieser Server ist voll! Kaufe dir &6Premium &7um ihn dennoch betreten zu können!"));
                    }
                }
            }
        }
        if(e.getTarget().getName().equalsIgnoreCase("Bauserver") || e.getTarget().getName().equalsIgnoreCase("View") || e.getTarget().getName().equalsIgnoreCase("Edit")) {
            if(!(c.getRang() == Rang.ARCHITEKT || (c.getRang().getId() >= Rang.SENIORMODERATOR.getId()))) {
                e.setCancelled(true);
            }
        }
    }

    private ProxiedPlayer getNonPremiumPlayer(List<ProxiedPlayer> players, Client join) {
        for(ProxiedPlayer player : players) {
            Client t = Main.getClient(player);
            if(t.getRang().getId() < join.getRang().getId()) {
                return player;
            }
        }
        return null;
    }
}
