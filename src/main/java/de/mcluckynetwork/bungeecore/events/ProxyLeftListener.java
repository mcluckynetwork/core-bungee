package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketFriendAction;
import de.mcluckynetwork.bungeecore.util.playersystems.FriendSystem;
import de.mcluckynetwork.bungeecore.util.playersystems.ReportSystem;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Robin on 31.07.2015.
 */
public class ProxyLeftListener implements Listener {

    @EventHandler
    public void onLeft(PlayerDisconnectEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Client c = Main.getClient(p);
        ChatClient.sendPacket(new PacketFriendAction(Main.name, p.getName(), PacketFriendAction.Action.QUIT, (ProxyServer.getInstance().getOnlineCount() - 1)));
        if(Main.online > 0) {
            Main.online = Main.online - 1;
        }
        if(Main.isPlayerOnProxy(p.getName())) {
            Main.getJedis().del(p.getName());
        }
        ReportSystem.clearReport(p);
        if(c.getRang().isTeam()) {
            long left = System.currentTimeMillis() / 1000;
            Main.getMySQL().Update("INSERT INTO onlineindex(uuid, join, leave, uptime) VALUES('" + c.getUuid() + "' , '" + c.getJoin() + "' , '" + left + "' , '" + (left - c.getJoin() + "')"));
        }
        /*if(PartyManager.isInParty(p)) {
            Party party = PartyManager.getParty(p);
            if(party != null) {
                if (party.getOwner().equals(p)) {
                    party.sendMessage(Main.ppre + Main.getClient(p).getChatName() + " §7hat §7die §7Party §7verlassen");
                    if (party.getMembers().size() > 1) {
                        Random r = new Random();
                        int owner = r.nextInt(party.getMembers().size());
                        ProxiedPlayer newowner = ProxyServer.getInstance().getPlayer(party.getMembers().get(owner));
                        party.sendMessage(Main.ppre + "§7Die §7Party §7hat §7eine §7neue §7Leitung: " + Main.getClient(newowner).getChatName());
                        party.removeMember(newowner);
                        party.setOwner(newowner);
                    } else {
                        party.sendMessage(Main.ppre + "§7Die Party wurde aufgelöst");
                        for (String m : party.getMembers()) {
                            PartyManager.parties.remove(m);
                        }
                    }
                    PartyManager.parties.remove(p.getName());
                } else {
                    if (party.getMembers().size() > 1) {
                        party.sendMessage(Main.ppre + Main.getClient(p).getChatName() + " §7hat §7die §7Party §7verlassen");
                        party.removeMember(p);
                        PartyManager.parties.remove(p.getName());
                    } else {
                        party.sendMessage(Main.ppre + "§7Die §7Party §7wurde §7aufgelöst");
                        party.removeMember(p);
                        PartyManager.parties.remove(p.getName());
                    }
                }
            }else{
                p.sendMessage(new TextComponent(Main.pre + "§7"));
            }
        }*/
        for(FriendSystem.Friend all : FriendSystem.getFriends(p.getName())) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(all.getName());
            if(target != null) {
                target.sendMessage(new TextComponent(Main.fpre + c.getChatName() + " §7ist §7nun §coffline"));
            }
        }
        Main.removeClient(p);
    }
}
