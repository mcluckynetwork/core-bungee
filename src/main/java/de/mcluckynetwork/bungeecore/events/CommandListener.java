package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Robin on 05.12.2015.
 */
public class CommandListener implements Listener {

    @EventHandler
    public void onPerformCommand(PlayerPerformCommandEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Client c = Main.getClient(p);
        if(c.isMuted()) {
            c.getMc().sendMessage(p);
            e.setCancelled(true);
        }else{
            if(e.getCommand().equalsIgnoreCase(""))
            if(e.getCommand().equalsIgnoreCase("server")) {
                if(c.getRang().getId() < Rang.ARCHITEKT.getId()) {
                    e.setCancelled(true);
                }
            }
        }
    }

}
