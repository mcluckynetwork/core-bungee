package de.mcluckynetwork.bungeecore.events;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Event;

/**
 * Created by Robin on 05.12.2015.
 */

@Getter
public class PlayerPerformCommandEvent extends Event {

    private ProxiedPlayer player;
    private String command;
    private String[] args;
    @Setter
    private boolean isCancelled;

    public PlayerPerformCommandEvent(ProxiedPlayer player, String command, String[] args) {
        this.player = player;
        this.command = command;
        this.args = args;
        this.isCancelled = false;
    }

}
