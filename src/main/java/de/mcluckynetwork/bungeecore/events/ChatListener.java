package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;

/**
 * Created by Robin on 24.07.2015.
 */
public class ChatListener implements Listener {

    public HashMap<String, String> lastmessage = new HashMap<>();

    @EventHandler
    public void onChat(ChatEvent e) {
        if(e.getSender() instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) e.getSender();
            Client c = Main.getClient(p);
            if(e.getMessage().equalsIgnoreCase("") || e.getMessage().equalsIgnoreCase(" ")) {
                return;
            }
            if(e.getMessage().startsWith("/")) {
                String[] cmd = e.getMessage().split(" ");
                String[] args = new String[cmd.length - 1];
                for(int i = 1; i < cmd.length; i++) {
                    int t  = i - 1;
                    args[t] = cmd[i];
                }
                PlayerPerformCommandEvent playerPerformCommandEvent = new PlayerPerformCommandEvent(p, cmd[0].replace("/", ""), args);
                ProxyServer.getInstance().getPluginManager().callEvent(playerPerformCommandEvent);
                if(playerPerformCommandEvent.isCancelled()) {
                    e.setCancelled(true);
                }
                return;
            }
            if(c.isMuted()) {
                e.setCancelled(true);
                c.getMc().sendMessage(p);
            }else {
                if (isLastMessage(p.getName(), e.getMessage())) {
                    e.setCancelled(true);
                    p.sendMessage(new TextComponent("§c>> §7Bitte wiederhole dich nicht"));
                } else {
                    String[] msg = e.getMessage().split(" ");
                    for (String a : Main.blacklist) {
                        for (int i = 0; i < msg.length; i++) {
                            if (msg[0].equalsIgnoreCase(a)) {
                                e.setCancelled(true);
                                p.sendMessage(new TextComponent("§c>> §7Bitte achte auf deine Wortwahl"));
                                return;
                            }
                        }
                    }
                    lastmessage.remove(p.getName());
                    lastmessage.put(p.getName(), e.getMessage());
                }
            }
        }
    }

    public boolean isLastMessage(String player, String name) {
        if(lastmessage.containsKey(player))  {
            String value = lastmessage.get(player);
            if(value.equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

}
