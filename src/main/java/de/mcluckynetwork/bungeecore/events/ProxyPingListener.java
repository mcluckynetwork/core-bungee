package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Robin on 25.07.2015.
 */
public class ProxyPingListener implements Listener {

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yy,HH:mm:ss");

    @EventHandler
    public void onPing(ProxyPingEvent e) {
        if(Main.state == ProxyState.MAINTENANCE) {
            ServerPing.Players p = e.getResponse().getPlayers();
            p = new ServerPing.Players(0, 0, p.getSample());
            ServerPing conn = new ServerPing(e.getResponse().getVersion(), p, "§eLuckyWorld.eu §7| §aLuckyWorld Netzwerk \n§cWartungsmodus", Main.f);
            conn.setVersion(new ServerPing.Protocol("§cWartungsarbeiten", 2));
            e.setResponse(conn);
        }else{
            if(e.getConnection() != null && e.getConnection().getVirtualHost() != null) {
                if (e.getConnection().getVirtualHost().getHostName() != null) {
                    if (e.getConnection().getVirtualHost().getHostName().contains("luckynetwork")) {
                        ServerPing conn = new ServerPing(e.getResponse().getVersion(), e.getResponse().getPlayers(), "§eLuckyWorld.eu §7| §aLuckyWorld Netzwerk \n§7Neue Domain: §eLuckyWorld.eu", Main.f);
                        conn.setVersion(new ServerPing.Protocol("§cVeraltet", 2));
                        e.setResponse(conn);
                    } else {
                        ServerPing.Players p = e.getResponse().getPlayers();
                        p = new ServerPing.Players(Main.maximal, Main.online, p.getSample());
                        e.setResponse(new ServerPing(e.getResponse().getVersion(), p, "§eLuckyWorld.eu §7| §aLuckyWorld Netzwerk \n" + Main.motd, Main.f));
                    }
                }
            }
        }
    }
}
