package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import de.mcluckynetwork.bungeecore.util.playersystems.BanSystem;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Robin on 24.07.2015.
 */
public class LoginListener implements Listener{

    private Random random = new Random();

    @EventHandler
    public void onLogin(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        if(!BanSystem.isInBanTable(uuid)) {
            Client c = Main.addClient(e.getConnection().getName(), uuid);
            if(Main.state != ProxyState.MAINTENANCE) {
                if(!(Main.wartungen.contains(uuid) || c.getRang().isTeam())) {
                    if(Main.online >= Main.maximal) {
                        if(c.getRang() == Rang.SPIELER) {
                            int rnd = random.nextInt(10);
                            if(!(rnd <= 2)) {
                                e.setCancelled(true);
                                e.setCancelReason("§7Das §6LuckyWorld §7Netzwerk §7ist §7leider §7voll\n§7Um es dennoch betreten zu können kannst du dir §6Premium §7kaufen\n\n§aShop: §7http://LuckyWorld.eu/shop");
                            }
                        }
                    }
                }
            }else{
                if(!(c.getRang().isTeam() || Main.wartungen.contains(uuid))) {
                    e.setCancelled(true);
                    e.setCancelReason("§7Du kannst das Netzwerk momentan nicht betreteten \n§cGrund: §cWartungsarbeiten");
                }
            }
        }else{
            e.setCancelled(true);
            e.setCancelReason(BanSystem.disconnectBanMessage(uuid, e.getConnection().getName()));
        }
    }

}
