package de.mcluckynetwork.bungeecore.events;

import de.mcluckynetwork.bungeecore.Main;
import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.Lobby;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.LobbyManager;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Robin on 03.09.2015.
 */
public class KickListener implements Listener {

    @EventHandler
    public void onKick(ServerKickEvent e) {
        ProxiedPlayer p = e.getPlayer();
        Client c = Main.getClient(p);
        String s = e.getKickReasonComponent()[0].toLegacyText();
        String[] kick = s.split(";");
        if(kick[0].equalsIgnoreCase("Kick_Cause_Restart")) {
            p.sendMessage(new TextComponent("1"));
            p.connect(ProxyServer.getInstance().getServerInfo(getNotLobby(kick[1]).getName()));
            return;
        }else if(kick[0].equalsIgnoreCase("Premium")) {
            ProxyServer.getInstance().broadcast(new TextComponent("JAWHOHL!!!!!!!!!"));
            p.sendMessage(new TextComponent(Main.pre + "§7Du §7wurdest §7für §7einen §7Spieler §7höheren §7Ranges §7gekickt"));
            if(c.getRang() == Rang.SPIELER) {
                p.sendMessage(new TextComponent(Main.pre + "§7Hole §7dir §7Premium §7um §7dies §7zu §7vermeiden!"));
                p.sendMessage(new TextComponent(Main.pre + "§7Unser §7Shop: §ahttp://LuckyWorld.eu/shop" ));
            }
        }else if(kick[0].equalsIgnoreCase("ConnectTo")) {
            String server = kick[1];
            p.connect(ProxyServer.getInstance().getServerInfo(server));
        }else if(kick[0].equalsIgnoreCase("Hack")) {
            String reason = kick[1];
            p.disconnect(new TextComponent(Main.pre + "§cDu wurdest automatisch gebannt! \n§7Grund: §e" + reason));
        }
        e.setCancelled(true);
        e.setCancelServer(ProxyServer.getInstance().getServerInfo(LobbyManager.getRandomLobby().getName()));
    }

    public static Lobby getNotLobby(String name) {
        Lobby l = LobbyManager.getRandomLobby();
        if(l.getName().equalsIgnoreCase(name)) {
            return getNotLobby(name);
        }
        return l;
    }

}
