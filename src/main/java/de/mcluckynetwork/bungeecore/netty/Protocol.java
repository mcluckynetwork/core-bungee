package de.mcluckynetwork.bungeecore.netty;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketActionBarUpdate;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerOnline;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerPlayerUpdate;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerStatusUpdate;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.*;
import lombok.Getter;

/**
 * Created by Robin on 29.11.2015.
 */
@Getter
public enum Protocol {

    PACKETSERVERONLINE(1, PacketServerOnline.class),

    PACKETSERVERPLAYERUPDATE(2, PacketServerPlayerUpdate.class),

    PACKETSERVERSTATUSUPDATE(3, PacketServerStatusUpdate.class),

    PACKETBROADCASTMESSAGE(4, PacketBroadcastMessage.class),

    PACKETFRIENDACTION(5, PacketFriendAction.class),

    PACKETFRIENDMSG(6, PacketFriendMsg.class),

    PACKETMOTDUPDATE(7, PacketMotdUpdate.class),

    PACKETPLAYERACTION(8, PacketPlayerAction.class),

    PACKETTEAMMESSAGE(9, PacketTeamMessage.class),

    PACKETFRIENDACTIONPERFORM(10, PacketFriendActionPerform.class),

    PACKETSLOTUPDATE(11, PacketSlotUpdate.class),

    PACKETBMMESSAGE(12, PacketBMMessage.class),

    PACKETSERVERREQUEST(13, PacketServerRequest.class),

    PACKETACTIONBARUPDATE(14, PacketActionBarUpdate.class),

    PACKETNETWORKPLAYERONLINE(15, PacketNetworkPlayerOnline.class),

    PACKETNETWORKSTOP(16, PacketNetworkStop.class),

    PACKETPLAYERREPORT(17, PacketPlayerReport.class),

    PACKETGLOBALSTATE(18, PacketGlobalState.class),

    PACKETPLAYERREPORTRAW(19, PacketPlayerReportJoin.class),

    PACKETSERVERINFO(20, PacketServerInfo.class),

    PACKETPERFORMCOMMAND(21, PacketPerformCommand.class),

    PACKETBLACKLIST(22, PacketBlackList.class);

    private int id;
    private Class<? extends Packet> packetClass;

    private Protocol(int id, Class<? extends Packet> packetClass)   {
        this.id = id;
        this.packetClass = packetClass;
    }

    public static Class<? extends Packet> getClassFromID(int id) {
        for(Protocol protocol : values()) {
            if(protocol.getId() == id) {
                return protocol.getPacketClass();
            }
        }
        return null;
    }

    public static int getIDFromClass(Class<? extends Packet> packetClass) {
        for(Protocol protocol : values()) {
            if(protocol.getPacketClass().equals(packetClass)) {
                return protocol.getId();
            }
        }
        return -1;
    }

    public int getId() {
        return this.id;
    }

    public Class<? extends Packet> getPacketClass() {
        return this.packetClass;
    }

}
