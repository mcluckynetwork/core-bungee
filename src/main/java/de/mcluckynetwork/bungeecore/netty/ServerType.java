package de.mcluckynetwork.bungeecore.netty;

import lombok.Getter;

/**
 * Created by Robin on 13.09.2015.
 */
public enum ServerType {

    GAME(1), LOBBY(2), PREMIUMLOBBY(3), SILENT(4), BUILD(5), PROXY(6);

    @Getter
    private int id;

    private ServerType(int id) {
        this.id = id;
    }

    public static ServerType fromID(int id) {
        for(ServerType t : values()) {
            if(t.getId() == id) {
                return t;
            }
        }
        return null;
    }
    public int getId(){
        return this.id;
    }
}
