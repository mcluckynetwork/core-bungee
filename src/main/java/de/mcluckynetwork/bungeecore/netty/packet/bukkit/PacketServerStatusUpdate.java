package de.mcluckynetwork.bungeecore.netty.packet.bukkit;

import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import lombok.Getter;

/**
 * Created by Robin on 13.09.2015.
 */
@Getter
public class PacketServerStatusUpdate extends Packet {

    private String name;
    private ServerType serverType;
    private State state;

    public PacketServerStatusUpdate() {

    }

    public PacketServerStatusUpdate(String name, ServerType type, State state) {
        this.name = name;
        this.serverType = type;
        this.state = state;
    }

    @Override
    public String write() {
        return serverType.getId() + ";" + name + ";" +  this.state.getID();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.serverType = ServerType.fromID(Integer.valueOf(msg[0]));
        this.name = msg[1];
        this.state = State.fromID(Integer.valueOf(msg[2]));
    }

}
