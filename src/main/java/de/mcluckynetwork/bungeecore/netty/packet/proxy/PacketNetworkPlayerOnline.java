package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 08.12.2015.
 */
@Getter
public class PacketNetworkPlayerOnline extends Packet {

    private int online;

    public PacketNetworkPlayerOnline() {

    }

    public PacketNetworkPlayerOnline(int online) {
        this.online = online;
    }

    @Override
    public String write() {
        return "On;" + online;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.online = Integer.valueOf(msg[1]);
    }

}
