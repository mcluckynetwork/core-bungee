package de.mcluckynetwork.bungeecore.netty.packet.bukkit;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 06.12.2015.
 */
@Getter
public class PacketActionBarUpdate extends Packet {

    private String actionbar;

    public PacketActionBarUpdate() {

    }

    public PacketActionBarUpdate(String actionbar) {
        this.actionbar = actionbar;
    }

    @Override
    public String write() {
        return actionbar;
    }

    @Override
    public void read(String in) {
        this.actionbar = in;
    }

}
