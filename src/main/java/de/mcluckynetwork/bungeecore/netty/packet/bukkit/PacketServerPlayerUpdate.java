package de.mcluckynetwork.bungeecore.netty.packet.bukkit;

import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 13.09.2015.
 */
@Getter
public class PacketServerPlayerUpdate extends Packet {

    private String name;
    private ServerType serverType;
    private int online;

    public PacketServerPlayerUpdate() {

    }

    public PacketServerPlayerUpdate(String name, ServerType serverType, int online) {
        this.name = name;
        this.serverType = serverType;
        this.online = online;
    }

    @Override
    public String write() {
        return serverType.getId() + ";" + name + ";" + this.online;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.serverType = ServerType.fromID(Integer.valueOf(msg[0]));
        this.name = msg[1];
        this.online = Integer.valueOf(msg[2]);
    }
}
