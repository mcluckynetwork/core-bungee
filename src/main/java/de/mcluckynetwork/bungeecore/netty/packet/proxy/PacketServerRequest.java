package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 04.12.2015.
 */
@Getter
@Setter
public class PacketServerRequest extends Packet {

    private String proxy;
    private String player;
    private String server;
    private ServerAction action;
    private String receiver;

    public PacketServerRequest() {

    }

    public PacketServerRequest(String proxy, String player, String server, ServerAction action, String receiver) {
        this.proxy = proxy;
        this.player = player;
        this.server = server;
        this.action = action;
        this.receiver = receiver;
    }

    @Override
    public String write() {
        return proxy + ";" + player + ";" + server + ";" + action.getId() + ";" + receiver;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.player = msg[1];
        this.server = msg[2];
        this.action = ServerAction.getServerActionFromID(Integer.valueOf(msg[3]));
        this.receiver = msg[4];
    }

    @Getter
    public enum ServerAction {

        WHEREIS(1),

        GOTO(2);

        private int id;

        private ServerAction(int id) {
            this.id = id;
        }

        public static ServerAction getServerActionFromID(int id) {
            for(ServerAction action : values()) {
                if(action.getId() == id) {
                    return action;
                }
            }
            return null;
        }

    }

}
