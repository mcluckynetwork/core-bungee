package de.mcluckynetwork.bungeecore.netty.packet;

/**
 * Created by Robin on 29.11.2015.
 */
public abstract class Packet {

    public abstract String write();
    public abstract void read(String in);

}
