package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 30.11.2015.
 */
@Getter
public class PacketFriendMsg extends Packet {

    private String proxy;
    private String sender;
    private String receiver;
    private String message;

    public PacketFriendMsg() {

    }

    public PacketFriendMsg(String proxy, String sender, String receiver, String message) {
        this.proxy = proxy;
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    @Override
    public String write() {
        return proxy + ";" + sender + ";" + receiver + ";" + message;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.sender = msg[1];
        this.receiver = msg[2];
        this.message = msg[3];
    }

}
