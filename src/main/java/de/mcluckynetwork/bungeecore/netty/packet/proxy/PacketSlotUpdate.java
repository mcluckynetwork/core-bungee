package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 04.12.2015.
 */
@Getter
public class PacketSlotUpdate extends Packet {

    private String proxy;
    private int slots;

    public PacketSlotUpdate() {

    }

    public PacketSlotUpdate(String proxy, int slots) {
        this.proxy = proxy;
        this.slots = slots;
    }

    @Override
    public String write() {
        return proxy + ";" + slots;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.slots = Integer.valueOf(msg[1]);
    }

}
