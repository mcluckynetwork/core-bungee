package de.mcluckynetwork.bungeecore.netty.packet.bukkit;

import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import lombok.Getter;

/**
 * Created by Robin on 13.09.2015.
 */
@Getter
public class PacketServerOnline extends Packet {

    private String name;
    private ServerType serverType;
    private State state;
    private int online;
    private int maximal;
    private String map;

    public PacketServerOnline() {

    }

    public PacketServerOnline(String name, ServerType serverType, State state, int online, int maximal, String map) {
        this.name = name;
        this.serverType = serverType;
        this.state = state;
        this.online = online;
        this.maximal = maximal;
        this.map = map;
    }

    @Override
    public String write() {
        return serverType.getId() + ";" + name + ";" + this.online + ";" + this.maximal + ";" + this.map + ";" + this.state.getID();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.serverType = ServerType.fromID(Integer.valueOf(msg[0]));
        this.name = msg[1];
        this.online = Integer.valueOf(msg[2]);
        this.maximal = Integer.valueOf(msg[3]);
        this.map = msg[4];
        this.state = State.fromID(Integer.valueOf(msg[5]));
    }

}
