package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.ProxyState;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 31.12.2015.
 */
@Getter
public class PacketGlobalState extends Packet {

    private String proxy;
    private ProxyState proxyState;

    public PacketGlobalState() {

    }

    public PacketGlobalState(String proxy, ProxyState state) {
        this.proxy = proxy;
        this.proxyState = state;
    }

    @Override
    public String write() {
        return proxy + ";" + proxyState.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.proxyState = ProxyState.getProxyStatefromID(Integer.valueOf(msg[1]));
    }

}

