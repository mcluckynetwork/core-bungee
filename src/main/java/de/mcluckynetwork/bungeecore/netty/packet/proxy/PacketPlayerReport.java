package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 30.12.2015.
 */
@Getter
public class PacketPlayerReport extends Packet {

    private String proxy;
    private String reported;
    private String reporter;
    private String reason;
    private int total;
    private int runtime;

    public PacketPlayerReport() {

    }

    public PacketPlayerReport(String proxy, String reported, String reporter, String reason, int total, int runtime) {
        this.proxy = proxy;
        this.reported = reported;
        this.reporter = reporter;
        this.reason = reason;
        this.total = total;
        this.runtime = runtime;
    }

    @Override
    public String write() {
        return proxy + ";" + reported + ";" + reporter + ";" + reason + ";" + total + ";" + runtime;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.reported = msg[1];
        this.reporter = msg[2];
        this.reason = msg[3];
        this.total = Integer.valueOf(msg[4]);
        this.runtime = Integer.valueOf(msg[5]);
    }
}
