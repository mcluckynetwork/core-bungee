package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 01.12.2015.
 */
@Getter
public class PacketBroadcastMessage extends Packet {

    private String proxy;
    private String message;

    public PacketBroadcastMessage() {

    }

    public PacketBroadcastMessage(String proxy, String message) {
        this.proxy = proxy;
        this.message = message;
    }

    @Override
    public String write() {
        return proxy + ";" + message;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.message = msg[1];
    }

}

