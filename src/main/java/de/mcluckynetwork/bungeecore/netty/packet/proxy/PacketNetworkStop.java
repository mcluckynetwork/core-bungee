package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 29.12.2015.
 */
@Getter
public class PacketNetworkStop extends Packet {

    private String network;

    public PacketNetworkStop() {

    }

    public PacketNetworkStop(String network) {
        this.network = network;
    }

    @Override
    public String write() {
        return "Restart;Network";
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.network = msg[1];
    }

}
