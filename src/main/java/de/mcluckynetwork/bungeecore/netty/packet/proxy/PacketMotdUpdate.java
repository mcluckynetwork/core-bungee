package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 01.12.2015.
 */
@Getter
public class PacketMotdUpdate extends Packet {

    private String proxy;
    private String motd;

    public PacketMotdUpdate() {

    }

    public PacketMotdUpdate(String proxy, String motd) {
        this.proxy = proxy;
        this.motd = motd;
    }

    @Override
    public String write() {
        return proxy + ";" + motd;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.motd = msg[1];
    }

    private enum Action{
        JOIN(1),

        QUIT(2);

        private int id;

        private Action(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static Action getActionFromID(int id) {
            for(Action ac : values()) {
                if(ac.getId() == id) {
                    return ac;
                }
            }
            return null;
        }

    }

}

