package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 01.01.2016.
 */
@Getter
public class PacketServerInfo extends Packet {

    private String proxy;
    private String name;
    private String host;
    private int port;
    private ServerAction action;

    public PacketServerInfo() {

    }

    public PacketServerInfo(String proxy, String name, String host, int port, ServerAction action) {
        this.proxy = proxy;
        this.name = name;
        this.host = host;
        this.port = port;
        this.action = action;
    }

    @Override
    public String write() {
        return proxy + ";" + name + ";" + host + ";" + port + ";" + action.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.name = msg[1];
        this.host = msg[2];
        this.port = Integer.valueOf(msg[3]);
        this.action = ServerAction.getActionFromID(Integer.valueOf(msg[4]));
    }

    public enum ServerAction{
        ADD(1),

        REMOVE(2);

        private int id;

        private ServerAction(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static ServerAction getActionFromID(int id) {
            for(ServerAction ac : values()) {
                if(ac.getId() == id) {
                    return ac;
                }
            }
            return null;
        }

    }

}
