package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 31.12.2015.
 */
@Getter
public class PacketPlayerReportJoin extends Packet {

    private String proxy;
    private String reported;
    private int total;

    public PacketPlayerReportJoin() {

    }

    public PacketPlayerReportJoin(String proxy, String reported, int total) {
        this.proxy = proxy;
        this.reported = reported;
        this.total = total;
    }

    @Override
    public String write() {
        return proxy + ";" + reported + ";" + total;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.reported = msg[1];
        this.total = Integer.valueOf(msg[2]);
    }
}
