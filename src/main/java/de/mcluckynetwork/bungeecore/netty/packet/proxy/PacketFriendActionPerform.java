package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 04.12.2015.
 */
@Getter
public class PacketFriendActionPerform extends Packet {

    private String proxy;
    private String sender;
    private String receiver;
    private FriendAction action;

    public PacketFriendActionPerform() {

    }

    public PacketFriendActionPerform(String proxy, String sender, String receiver, FriendAction action) {
        this.proxy = proxy;
        this.sender = sender;
        this.action = action;
        this.receiver = receiver;
    }

    @Override
    public String write() {
       return proxy + ";" + sender + ";" + receiver + ";" + action.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.sender = msg[1];
        this.receiver = msg[2];
        this.action = FriendAction.getActionFromID(Integer.valueOf(msg[3]));
    }

    public enum FriendAction{

        ADD(1),

        ACCEPT(2),

        DENY(3),

        REMOVE(4);

        private int id;

        private FriendAction(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static FriendAction getActionFromID(int id) {
            for(FriendAction ac : values()) {
                if(ac.getId() == id) {
                    return ac;
                }
            }
            return null;
        }

    }

}
