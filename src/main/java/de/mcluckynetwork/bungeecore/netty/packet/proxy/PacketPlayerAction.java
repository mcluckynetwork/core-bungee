package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 01.12.2015.
 */
@Getter
public class PacketPlayerAction extends Packet {

    private String proxy;
    private String reason;
    private String player;
    private PlayerAction action;

    public PacketPlayerAction() {

    }

    public PacketPlayerAction(String proxy, String player, String reason, PlayerAction action) {
        this.proxy = proxy;
        this.reason = reason;
        this.player = player;
        this.action = action;
    }

    @Override
    public String write() {
        return proxy + ";" + player + ";" + reason + ";" + action.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.player = msg[1];
        this.reason = msg[2];
        this.action = PlayerAction.getActionFromID(Integer.valueOf(msg[3]));
    }

    public enum PlayerAction{
        KICK(1),

        BAN(2),

        MUTE(3);

        private int id;

        private PlayerAction(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static PlayerAction getActionFromID(int id) {
            for(PlayerAction ac : values()) {
                if(ac.getId() == id) {
                    return ac;
                }
            }
            return null;
        }

    }

}


