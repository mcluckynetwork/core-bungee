package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 06.01.2016.
 */
@Getter
public class PacketPerformCommand extends Packet {

    private String sender;
    private String command;

    public PacketPerformCommand() {

    }

    public PacketPerformCommand(String sender, String command) {
        this.sender = sender;
        this.command = command;
    }

    @Override
    public String write() {
        return sender + ";" + command;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.sender = msg[0];
        this.command = msg[1];
    }

}
