package de.mcluckynetwork.bungeecore.netty.packet.bukkit;

import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 30.11.2015.
 */
@Getter
public class PacketServerMapUpdate extends Packet {

    private String name;
    private ServerType serverType;
    private String map;

    public PacketServerMapUpdate() {

    }

    public PacketServerMapUpdate(String name, ServerType serverType, String map) {
        this.name = name;
        this.serverType = serverType;
        this.map = map;
    }

    @Override
    public String write() {
        return serverType.getId() + ";" + name + ";" + this.map;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.serverType = ServerType.fromID(Integer.valueOf(msg[0]));
        this.name = msg[1];
        this.map = msg[2];
    }

}
