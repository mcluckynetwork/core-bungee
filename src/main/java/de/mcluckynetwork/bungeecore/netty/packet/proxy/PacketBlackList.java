package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import lombok.Getter;

/**
 * Created by Robin on 09.01.2016.
 */
@Getter
public class PacketBlackList extends Packet {

    private String proxy;
    private String message;
    private BlackListAction action;

    public PacketBlackList() {

    }

    public PacketBlackList(String proxy, String message, BlackListAction action) {
        this.proxy = proxy;
        this.message = message;
        this.action = action;
    }

    @Override
    public String write() {
        return proxy + ";" + message + ";" + action.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.message = msg[1];
        this.action = BlackListAction.getBlackListActionFROMID(Integer.valueOf(msg[2]));
    }

    public enum BlackListAction{

        ADD(1),

        REMOVE(2);

        private int id;

        private BlackListAction(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static BlackListAction getBlackListActionFROMID(int id) {
            for(BlackListAction bm : values()) {
                if(bm.getId() == id) {
                    return bm;
                }
            }
            return null;
        }

    }

}