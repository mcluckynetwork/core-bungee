package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 30.11.2015.
 */
@Getter
public class PacketFriendAction extends Packet {

    private String proxy;
    private String sender;
    private Action action;
    private int online;

    public PacketFriendAction() {

    }

    public PacketFriendAction(String proxy, String sender, Action action, int onlineplayers) {
        this.proxy = proxy;
        this.sender = sender;
        this.action = action;
        this.online = onlineplayers;
    }

    @Override
    public String write() {
        return proxy + ";" + sender + ";" + action.getId() + ";" + online;
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.sender = msg[1];
        this.action = Action.getActionFromID(Integer.valueOf(msg[2]));
        this.online = Integer.valueOf(msg[3]);
    }

    public enum Action{
        JOIN(1),

        QUIT(2);

        private int id;

        private Action(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static Action getActionFromID(int id) {
            for(Action ac : values()) {
                if(ac.getId() == id) {
                    return ac;
                }
            }
            return null;
        }

    }

}
