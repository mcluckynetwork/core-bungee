package de.mcluckynetwork.bungeecore.netty.packet.proxy;

import de.mcluckynetwork.bungeecore.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.nio.charset.Charset;

/**
 * Created by Robin on 04.12.2015.
 */
@Getter
public class PacketBMMessage extends Packet {

    private String proxy;
    private String message;
    private BMAction action;

    public PacketBMMessage() {

    }

    public PacketBMMessage(String proxy, String message, BMAction action) {
        this.proxy = proxy;
        this.message = message;
        this.action = action;
    }

    @Override
    public String write() {
        return proxy + ";" + message + ";" + action.getId();
    }

    @Override
    public void read(String in) {
        String[] msg = in.split(";");
        this.proxy = msg[0];
        this.message = msg[1];
        this.action = BMAction.getBMAactionFROMID(Integer.valueOf(msg[2]));
    }

    public enum BMAction{

        ADD(1),

        REMOVE(2);

        private int id;

        private BMAction(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static BMAction getBMAactionFROMID(int id) {
            for(BMAction bm : values()) {
                if(bm.getId() == id) {
                    return bm;
                }
            }
            return null;
        }

    }

}
