package de.mcluckynetwork.bungeecore.netty;

import lombok.Getter;

/**
 * Created by Robin on 31.12.2015.
 */
@Getter
public enum ProxyState {

    ALPHA(0, "alpha"),

    BETA(1, "beta"),

    FINAL(2, "final"),

    MAINTENANCE(3, "wartung");

    private int id;
    private String state;

    private ProxyState(int id, String state) {
        this.id = id;
        this.state = state;
    }

    public static ProxyState getProxyStatefromID(int id) {
        for(ProxyState state : values()) {
            if(state.getId() == id) {
                return state;
            }
        }
        return MAINTENANCE;
    }

    public static ProxyState getProxyStatefromState(String state) {
        for(ProxyState proxyState : values()) {
            if(proxyState.getState().equalsIgnoreCase(state)) {
                return proxyState;
            }
        }
        return MAINTENANCE;
    }

}
