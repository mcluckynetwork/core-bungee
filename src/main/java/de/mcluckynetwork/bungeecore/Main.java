package de.mcluckynetwork.bungeecore;

import de.mcluckynetwork.bungeecore.clients.Client;
import de.mcluckynetwork.bungeecore.clients.Rang;
import de.mcluckynetwork.bungeecore.commands.*;
import de.mcluckynetwork.bungeecore.util.communication.ChatClient;
import de.mcluckynetwork.bungeecore.events.*;
import de.mcluckynetwork.bungeecore.netty.ProxyState;
import de.mcluckynetwork.bungeecore.netty.ServerType;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerOnline;
import de.mcluckynetwork.bungeecore.netty.packet.bukkit.PacketServerStatusUpdate;
import de.mcluckynetwork.bungeecore.netty.packet.proxy.PacketTeamMessage;
import de.mcluckynetwork.bungeecore.util.BroadcastMessage;
import de.mcluckynetwork.bungeecore.util.UUIDFetcher;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanManager;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanProfile;
import de.mcluckynetwork.bungeecore.util.playersystems.bansystem.BanType;
import de.mcluckynetwork.bungeecore.util.serversystems.lobbysystem.State;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import redis.clients.jedis.Jedis;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Robin on 22.07.2015.
 */

public class Main extends Plugin {

    public static HashMap<String, Client> clients = new HashMap<>();
    public static List<Client> team = new ArrayList<>();
    public static List<Client> debug = new ArrayList<>();
    public static List<Client> error = new ArrayList<>();

    public static String motd = null;
    public static String pre = "§7[§6LuckyWorld§7] ";
    public static String ppre = "§7[§9Party§7] ";
    public static String fpre = "§7[§bFreunde§7] ";
    public static String rpre = "§7[§cReport§7] ";

    public static ArrayList<String> blacklist = new ArrayList<>();
    public static List<UUID> wartungen = new ArrayList<>();
    public static ProxyState state;
    private static Main instance;
    private static MySQL mysql;
    private static Jedis jedis;
    public static Favicon f = null;

    public static List<BroadcastMessage> messages = new ArrayList<>();
    public static int broadcast = 0;
    public static int online = 0;
    public static int maximal;

    public static String name;

    @Override
    public void onEnable() {
        instance = this;
        mysql = new MySQL("root", "aewSjcj5Gm1O64y1ps4QqlV733QTCyT7nuEC2d78", "localhost", "Netzwerk");
        jedis = new Jedis("localhost");
        try {
            new ChatClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int p = ((ListenerInfo)ProxyServer.getInstance().getConfig().getListeners().toArray()[0]).getHost().getPort();
        name = "Proxy" + (p - 25000);
        ChatClient.sendPacket(new PacketServerOnline(name, ServerType.PROXY, State.ONLINE, 0, 200, ""));
        setDatas();
        registerCommands();
        registerEvents();
        mysql.Update("CREATE TABLE IF NOT EXISTS nreports(uuid VARBINARY(100), total INT, runtime INT)");
        mysql.Update("CREATE TABLE IF NOT EXISTS banprofiles(id INT, name TEXT, time INT, bantype INT, videoid INT)");
        mysql.Update("CREATE TABLE IF NOT EXISTS bantypes(id INT, name TEXT, banprofile INT)");
        mysql.Update("CREATE TABLE IF NOT EXISTS onlineindex(uuid VARBINARY(100), join INT, leave INT, uptime INT)");
        try {
            ResultSet rs = getMySQL().Query("SELECT * FROM blacklist");
            while(rs.next()) {
                String word = rs.getString("word");
                Main.blacklist.add(word);
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        try {
            f = Favicon.create(ImageIO.read(new URL("http://185.87.22.156/server-icon.png")));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        setupMessages();
        startMessageSending();
        setupServer();
        importBanProfiles();
        importBanTypes();
    }

    public void setDatas() {
        ResultSet rs = getMySQL().Query("SELECT * FROM manager");
        try{
            while(rs.next()) {
                String first = rs.getString("first");
                if (first.equalsIgnoreCase("motd")) {
                    Main.motd = rs.getString("second");
                } else if (first.equalsIgnoreCase("state")) {
                    Main.state = ProxyState.getProxyStatefromID(Integer.valueOf(rs.getString("second")));
                }else if(first.equalsIgnoreCase("maximal")) {
                    Main.maximal = Integer.valueOf(rs.getString("second"));
                }
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void importBanProfiles() {
        try {
            ResultSet rs = getMySQL().Query("SELECT * FROM banprofiles");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int time = rs.getInt("time");
                boolean bantype = BanManager.getBooleanFromInt(rs.getInt("bantype"));
                boolean videoid = BanManager.getBooleanFromInt(rs.getInt("videoid"));
                BanManager.addBanProfile(new BanProfile(id, name, time, bantype, videoid));
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void importBanTypes() {
        try{
            ResultSet rs = getMySQL().Query("SELECT * FROM bantypes");
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                BanProfile profile = BanManager.getBanProfile(rs.getInt("banprofile"));
                BanManager.addBanType(new BanType(id, name, profile));
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        ChatClient.sendPacket(new PacketServerStatusUpdate(name, ServerType.PROXY, State.OFFLINE));
    }

    public void setupMessages() {
        ResultSet rs = mysql.Query("SELECT * FROM messages");
        try{
            while(rs.next()) {
                String msg = rs.getString("message");
                messages.add(new BroadcastMessage(messages.size(), msg));
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void registerCommands() {
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ABCommand("ab"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanCommand("ban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanLogCommand("banlog"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanProfilesCommand("banprofiles"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanTypesCommand("bantypes"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BelastendCommand("belastend"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BlacklistCommand("blacklist"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BMCommand("bm"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BroadcastCommand("bc"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ChatlogCommand("chatlog"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ColorCommand("color"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ErrorCommand("error"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new FriendCommand("friend"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GoToCommand("goto"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GStatusCommand("gstatus"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HelpCommand("help"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("hub"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("lobby"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("l"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new InfoCommand("info"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new KickCommand("kick"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MBanCommand("mban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MotdCommand("motd"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MsgCommand("msg"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MsgCommand("fc"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MMuteCommand("mute"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new NetworkStopCommand("networkstop"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new NotifyCommand("notify"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PartyCommand("party"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PCommand("p"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PCommand("pc"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PingCommand("ping"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ProxyCommand("proxy"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new RCommand("r"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ReportCommand("report"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ServerInfoCommand("serverinfo"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new RankCommand("rank"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new SlotsCommand("slots"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StatusCommand("status"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new TBanCommand("tban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new TeamChatCommand("t"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new UnbanCommand("unban"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new UnmuteCommand("unmute"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new UUIDCommand("uuid"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new WartungCommand("wartung"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new WhereIsCommand("whereis"));
    }

    public void registerEvents() {
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ChatListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new KickListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new LoginListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PostLoginListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ProxyLeftListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ProxyPingListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerConnectListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ServerConnectedListener());
    }

    public static Jedis getJedis() {
        if(jedis.isConnected()) {
            return jedis;
        }else{
            jedis.connect();
            if(jedis.isConnected()) {
                return jedis;
            }
        }
        return null;
    }

    public static void setupServer() {
        ResultSet rs = getMySQL().Query("SELECT * FROM bungeeserver");
        try{
            while(rs.next()) {
                String name = rs.getString("name");
                String host = rs.getString("host");
                int port = rs.getInt("port");
                ServerInfo s = ProxyServer.getInstance().constructServerInfo(name, new InetSocketAddress(host,port), "", false);
                ProxyServer.getInstance().getServers().put(name, s);
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void addServer(String name, String host, int port) {
        ServerInfo s = ProxyServer.getInstance().constructServerInfo(name, new InetSocketAddress(host, port), "", false);
        ProxyServer.getInstance().getServers().put(name, s);
        getMySQL().Update("INSERT INTO bungeeserver (name, host, port) VALUES ('" + name + "' , '" + host + "' , '" + port + "')");
    }

    public static boolean isServerInMySQL(String name) {
        ResultSet rs = getMySQL().Query("SELECT name FROM bungeeserver WHERE name='" + name + "'");
        try{
            if(rs.next()) {
                return true;
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static boolean isServerInMap(String name) {
        if(ProxyServer.getInstance().getServers().containsKey(name)) {
            return true;
        }
        return false;
    }

    public static void removeServer(String name) {
        if(ProxyServer.getInstance().getServers().containsKey(name)) {
            ProxyServer.getInstance().getServers().remove(name);
        }
        getMySQL().Update("DELETE FROM bungeeserver WHERE name='" + name + "'");
    }

    public static Main getInstance() {
        return instance;
    }

    public static MySQL getMySQL() {
        return mysql;
    }

    public static Client getClient(String name) {
        if(isPlayerOnline(name)) {
            if(clients.containsKey(name)) {
                return clients.get(name);
            }else{
                return addClient(name, ProxyServer.getInstance().getPlayer(name).getUniqueId());
            }
        }else{
            return null;
        }
    }

    public static Client getClient(ProxiedPlayer p) {
        return getClient(p.getName());
    }

    public static boolean isClientRegistered(UUID uuid) {
        try{
            ResultSet rs = Main.getMySQL().Query("SELECT rang FROM clients WHERE uuid='" + uuid + "'");
            if(rs.next()) {
                return true;
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static Client addClient(String name, UUID uuid)  {
        try {
            ResultSet rs = getMySQL().Query("SELECT * FROM clients WHERE uuid='" + uuid + "'");
            if (rs.next()) {
                Rang r = Rang.fromID(rs.getInt("rang"));
                if(r == null) {
                    r = Rang.SPIELER;
                }
                String settings = rs.getString("settings");
                ResultSet uuidfetch = getMySQL().Query("SELECT uuid FROM uuids WHERE name='" + name + "'");
                if(uuidfetch.next()) {
                    getMySQL().Update("INSERT INTO uuids(name, uuid) VALUES ('" + name + "' , '" + uuid + "')");
                }else {
                    getMySQL().Update("UPDATE uuids SET name='" + name + "' WHERE uuid='" + uuid + "'");
                }
                System.out.println("Loaded Client: " + uuid + "; " + r.getId() +  "; " + settings);
                char[] s = settings.toCharArray();
                Client c = new Client(uuid, name, r , s, true);
                clients.put(name, c);
                return c;
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        getMySQL().Update("INSERT INTO clients (uuid, rang, nicked, settings) VALUES ('" + uuid +  "' , '" + Rang.SPIELER.getId() + "' , '" + 0 + "' , '" + "fttff" + "')");
        getMySQL().Update("INSERT INTO uuids(uuid, name) VALUES('" + uuid + "' , '" + name + "')");
        String s = "fttff";
        char[] ch = s.toCharArray();
        Client c = new Client(uuid, name, Rang.SPIELER, ch, true);
        clients.put(name, c);
        return c;
    }

    public static Client getOnOfflineClient(UUID uuid, String name) {
        if(clients.containsKey(name)) {
            return getClient(name);
        }else{
            return getOnOfflineClient(uuid, name);
        }
    }

    public static Client getOfflineClient(UUID uuid, String name) {
        try {
            ResultSet rs = Main.getMySQL().Query("SELECT * FROM clients WHERE uuid='" + uuid + "'");
            if (rs.next()) {
                Rang r = Rang.fromID(rs.getInt("rang"));
                String settings = rs.getString("settings");
                char[] s = settings.toCharArray();
                Client c = new Client(uuid, UUIDFetcher.getName(uuid), r, s, false);
                return c;
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addClientToTeam(Client c) {
        for(Client cs : team) {
            if(cs.getName().equalsIgnoreCase(c.getName())) {
                return;
            }
        }
        team.add(c);
    }

    public static void addClientToError(Client c) {
        for(Client cs : error) {
            if(cs.getName().equalsIgnoreCase(c.getName())) {
                return;
            }
        }
        error.add(c);
    }

    public static void removeClient(ProxiedPlayer p) {
        Client c = getClient(p);
        clients.remove(p.getName());
        if(team.contains(c)) {
            team.remove(c);
        }
        if(error.contains(c)) {
            error.remove(c);
        }
        if(debug.contains(c)) {
            debug.remove(c);
        }
    }

    public static boolean isPlayerOnline(String name) {
        if(ProxyServer.getInstance().getPlayer(name) == null) {
            return false;
        }
        return true;
    }

    public static boolean isPlayerOnProxy(String name) {
        return getJedis().exists(name);
    }

    public static void sendTeamMessage(String name, String message) {
        ChatClient.sendPacket(new PacketTeamMessage(Main.name, name, message));
        if(name.equalsIgnoreCase("System")) {
            String n = "§7[§4System§7] ";
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    if(c.getRang() != Rang.ARCHITEKT) {
                        p.sendMessage(new TextComponent(n + message));
                    }
                }
            }
        }else{
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    p.sendMessage(new TextComponent("§7[§3Team§7] " + name + "§8: §7" + message));
                }
            }
        }
    }

    public static void sendRawTeamMessage(String name, String message) {
        if(name.equalsIgnoreCase("System")) {
            String n = "§7[§4System§7] ";
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    if(c.getRang() != Rang.ARCHITEKT) {
                        p.sendMessage(new TextComponent(n + message));
                    }
                }
            }
        }else{
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    p.sendMessage(new TextComponent("§7[§3Team§7] " + name + "§8: §7" + message));
                }
            }
        }
    }

    public static void sendTeamMessage(String name, TextComponent tc) {
        ChatClient.sendPacket(new PacketTeamMessage(Main.name, name, tc.toLegacyText()));
        if(name.equalsIgnoreCase("System")) {
            String n = "§7[§4System§7] ";
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    if(c.getRang() != Rang.ARCHITEKT) {
                        p.sendMessage(new TextComponent(n), tc);
                    }
                }
            }
        }else{
            for(Client c : team) {
                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
                if(p != null) {
                    p.sendMessage(new TextComponent("§7[§3Team§7] " + name + "§8: §7"), tc);
                }
            }
        }
    }

    public static void sendReportMessage(String message) {
        for(Client c : team) {
            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
            if(p != null) {
                if(c.getRang() != Rang.ARCHITEKT) {
                    p.sendMessage(new TextComponent(message));
                }
            }
        }
    }

    public static void sendReportMessage(TextComponent textComponent) {
        for(Client c : team) {
            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
            if(p != null) {
                if(c.getRang() != Rang.ARCHITEKT) {
                    p.sendMessage(textComponent);
                }
            }
        }
    }

    public static void sendErrorMessage(TextComponent tc) {
        for(Client c : error) {
            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
            if(p != null) {
                p.sendMessage(new TextComponent("§7[§4Error§7] "), tc);
            }
        }
    }

    public static void sendErrorMessage(String msg) {
        for(Client c : error) {
            ProxiedPlayer p = ProxyServer.getInstance().getPlayer(c.getName());
            if(p != null) {
                p.sendMessage(new TextComponent("§7[§4Error§7] " + msg));
            }
        }
    }

    public static void startMessageSending() {
        ProxyServer.getInstance().getScheduler().schedule(getInstance(), () -> {
            if(messages.size() == 0) {
                return;
            }
            int t = broadcast + 1;
            if(t > messages.size()) {
                broadcast = 0;
            }
            ProxyServer.getInstance().broadcast(new TextComponent("§7[§4INFO§7] §7" + messages.get(broadcast).getMessage()));
            broadcast = broadcast + 1;
        },0L, 2L, TimeUnit.MINUTES);
    }

}