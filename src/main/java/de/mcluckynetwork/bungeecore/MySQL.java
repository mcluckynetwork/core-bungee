package de.mcluckynetwork.bungeecore;

import java.sql.*;

/**
 * Created by Robin on 23.07.2015.
 */
public class MySQL {

    private String user;
    private String pass;
    private String host;
    private String db;
    private Connection connection;

    public MySQL(String user,String pass,String host,String db) {
        this.user=user;
        this.pass=pass;
        this.host=host;
        this.db=db;
        connect();
    }

    public void close(){
        try{
            if (connection != null) {
                connection.close();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + db, user, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void Update(String qry) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (Exception ex) {
            connect();
            ex.printStackTrace();
        }
    }

    public ResultSet Query(String qry) {
        ResultSet rs = null;
        try {
            Statement stmt = connection.createStatement();
            rs = stmt.executeQuery(qry);
        }catch (Exception ex) {
            connect();
            ex.printStackTrace();
        }
        return rs;
    }
}
